  /*
  * policy.c
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */

#include <stdint.h>
#include <syslog.h>
#include <unistd.h>
#include <signal.h>
#include <getopt.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <libgen.h>     // dirname() and basename()
#include <ctype.h>      // isdigit()	  
#include <errno.h>

#include <simple-netaid/sbuf.h>
#include <simple-netaid/ipaddr.h>
#include <simple-netaid/interfaces.h>
#include <simple-netaid/helpers.h>
#include <simple-netaid/wireless.h>

#include "def.h"
#include "policy.h"
#include "util.h"

#define WIFI_DIR "/etc/network/wifi"

static struct ubus_request_data req_data;
static struct blob_buf bb_t;

/* Enum for EGI policy order */
enum {
	INTERFACE_DOWN_ID,
	INTERFACE_DOWN_IFNAME,
	__INTERFACE_DOWN_MAX,
};
 
/* Ubus Policy */
const struct blobmsg_policy interface_down_policy[] =
{
	[INTERFACE_DOWN_ID]  = { .name="id", .type=BLOBMSG_TYPE_INT32 },
	[INTERFACE_DOWN_IFNAME] = { .name="ifname", .type=BLOBMSG_TYPE_STRING },
};

/* Enum for EGI policy order */
enum {
	INTERFACE_UP_ID,
	INTERFACE_UP_IFNAME,
	__INTERFACE_UP_MAX,
};
 
/* Ubus Policy */
const struct blobmsg_policy interface_up_policy[] =
{
	[INTERFACE_UP_ID]  = { .name="id", .type=BLOBMSG_TYPE_INT32 },
	[INTERFACE_UP_IFNAME] = { .name="ifname", .type=BLOBMSG_TYPE_STRING },
};

/* Enum for EGI policy order */
enum {
	IFDOWN_ID,
	IFDOWN_IFNAME,
	IFDOWN_TTY,
	__IFDOWN_MAX,
};
 
/* Ubus Policy */
const struct blobmsg_policy ifdown_policy[] =
{
	[IFDOWN_ID] = { .name="id", .type=BLOBMSG_TYPE_INT32 },
	[IFDOWN_IFNAME] = { .name="ifname", .type=BLOBMSG_TYPE_STRING },
	[IFDOWN_TTY] = { .name="tty", .type=BLOBMSG_TYPE_INT32 },
};

/* Enum for EGI policy order */
enum {
	IFUP_ID,
	IFUP_IFNAME,
	IFUP_TTY,
	__IFUP_MAX,
};
 
/* Ubus Policy */
const struct blobmsg_policy ifup_policy[] =
{
	[IFUP_ID] = { .name="id", .type=BLOBMSG_TYPE_INT32 },
	[IFUP_IFNAME] = { .name="ifname", .type=BLOBMSG_TYPE_STRING },
	[IFUP_TTY] = { .name="tty", .type=BLOBMSG_TYPE_INT32 },
};

/* Enum for EGI policy order */
enum {
	IPADDR_FLUSH_ID,
	IPADDR_FLUSH_IFNAME,
	__IPADDR_FLUSH_MAX,
};
 
/* Ubus Policy */
const struct blobmsg_policy ipaddr_flush_policy[] =
{
	[IPADDR_FLUSH_ID]  = { .name="id", .type=BLOBMSG_TYPE_INT32 },
	[IPADDR_FLUSH_IFNAME] = { .name="ifname", .type=BLOBMSG_TYPE_STRING },
};
 
/* Enum for EGI policy order */
enum {
	WPA_PASSPHRASE_ID,
	WPA_PASSPHRASE_ESSID,
	WPA_PASSPHRASE_PASSWD,
	WPA_PASSPHRASE_FILENAME,
	__WPA_PASSPHRASE_MAX,
};
 
/* Ubus Policy */
const struct blobmsg_policy wpa_passphrase_policy[] =
{
	[WPA_PASSPHRASE_ID]  = { .name="id", .type=BLOBMSG_TYPE_INT32 },
	[WPA_PASSPHRASE_ESSID] = { .name="essid", .type=BLOBMSG_TYPE_STRING },
	[WPA_PASSPHRASE_PASSWD] = { .name="passwd", .type=BLOBMSG_TYPE_STRING },
	[WPA_PASSPHRASE_FILENAME] = { .name="filename", .type=BLOBMSG_TYPE_STRING },
};
 
/* Enum for EGI policy order */
enum {
	WPA_SUPPLICANT_ID,
	WPA_SUPPLICANT_IFNAME,
	WPA_SUPPLICANT_FILENAME,
	WPA_SUPPLICANT_TTY,
	__WPA_SUPPLICANT_MAX,
};
 
/* Ubus Policy */
const struct blobmsg_policy wpa_supplicant_policy[] =
{
	[WPA_SUPPLICANT_ID]  = { .name="id", .type=BLOBMSG_TYPE_INT32 },
	[WPA_SUPPLICANT_IFNAME] = { .name="ifname", .type=BLOBMSG_TYPE_STRING },
	[WPA_SUPPLICANT_FILENAME] = { .name="filename", .type=BLOBMSG_TYPE_STRING },
	[WPA_SUPPLICANT_TTY]  = { .name="tty", .type=BLOBMSG_TYPE_INT32 },
};
 
/* Enum for EGI policy order */
enum {
	UNINSTALL_ID,
	UNINSTALL_FILENAME,
	__UNINSTALL_MAX,
};
 
/* Ubus Policy */
const struct blobmsg_policy uninstall_policy[] =
{
	[UNINSTALL_ID]  = { .name="id", .type=BLOBMSG_TYPE_INT32 },
	[UNINSTALL_FILENAME] = { .name="filename", .type=BLOBMSG_TYPE_STRING },
};

/* Enum for EGI policy order */
enum {
	SCAN_ACTIVE_WIFIS_ID,
	SCAN_ACTIVE_WIFIS_IFNAME,
	__SCAN_ACTIVE_WIFIS_MAX,
};
 
/* Ubus Policy */
const struct blobmsg_policy scan_active_wifis_policy[] =
{
	[SCAN_ACTIVE_WIFIS_ID]  = { .name="id", .type=BLOBMSG_TYPE_INT32 },
	[SCAN_ACTIVE_WIFIS_IFNAME] = { .name="ifname", .type=BLOBMSG_TYPE_STRING },
};
 
/* Ubus Methods */
const struct ubus_method netaid_methods[] =
{
	UBUS_METHOD("interface_down", interface_down_handler, interface_down_policy),
	UBUS_METHOD("interface_up", interface_up_handler, interface_up_policy),
	UBUS_METHOD("ifdown", ifdown_handler, ifdown_policy),
	UBUS_METHOD("ifup", ifup_handler, ifup_policy),
	UBUS_METHOD("ipaddr_flush", ipaddr_flush_handler, ipaddr_flush_policy),
	UBUS_METHOD("wpa_passphrase", wpa_passphrase_handler, wpa_passphrase_policy),
	UBUS_METHOD("wpa_supplicant", wpa_supplicant_handler, wpa_supplicant_policy),
	UBUS_METHOD("uninstall", uninstall_handler, uninstall_policy),
	UBUS_METHOD("scan_active_wifis", scan_active_wifis_handler, scan_active_wifis_policy),
};
 
/* Ubus object type */
struct ubus_object_type netaid_obj_type =
	UBUS_OBJECT_TYPE("ering.netaid", netaid_methods);
 
/* Ubus object */
struct ubus_object netaid_obj=
{
	.name = "ering.netaid", 	/* with APP name */
	.type = &netaid_obj_type,
	.methods = netaid_methods,
	.n_methods = ARRAY_SIZE(netaid_methods),
	//.path= /* useless */
};

/**
 * \brief Callback function for ubus interface_down method handling
 */
int interface_down_handler( struct ubus_context *ctx, struct ubus_object *obj,
			  struct ubus_request_data *req, const char *method,
			  struct blob_attr *msg_t )
{
   struct sbuf cmd;
   
   /* for parsed attr */
   struct blob_attr *tb[__INTERFACE_DOWN_MAX];
    
   /* Parse blob_msg from the caller to request policy */
   blobmsg_parse(interface_down_policy, ARRAY_SIZE(interface_down_policy), tb, blob_data(msg_t), blob_len(msg_t));

   const char *ifname = blobmsg_data(tb[INTERFACE_DOWN_IFNAME]);
   
   interface_down(ifname);
   
   sbuf_init(&cmd);
   sbuf_concat(&cmd, 3, "interface ", ifname, " down");
 
   /* send a reply msg to the caller for information */
   blob_buf_init(&bb_t, 0);
   blobmsg_add_string(&bb_t,"Server reply - Request has been proceeded: ", cmd.buf);
   ubus_send_reply(ctx, req, bb_t.head);
   sbuf_free(&cmd);
 
   /* 	-----  reply results to the caller ----- */
   ubus_defer_request(ctx, req, &req_data);
   ubus_complete_deferred_request(ctx, req, UBUS_STATUS_OK);

   return 0;
}

/**
 * \brief Callback function for ubus interface_up method handling
 */
int interface_up_handler( struct ubus_context *ctx, struct ubus_object *obj,
			  struct ubus_request_data *req, const char *method,
			  struct blob_attr *msg_t )
{
   char msg[64] = {0};
   
   /* for parsed attr */
   struct blob_attr *tb[__INTERFACE_UP_MAX];
    
   /* Parse blob_msg from the caller to request policy */
   blobmsg_parse(interface_up_policy, ARRAY_SIZE(interface_up_policy), tb, blob_data(msg_t), blob_len(msg_t));

   const char *ifname = blobmsg_data(tb[INTERFACE_DOWN_IFNAME]);
   
   interface_up(ifname);
    
   strncpy_t(msg, sizeof(msg), "interface ", 10);
   strcat(msg, ifname);
   strcat(msg, " up");
 
   /* send a reply msg to the caller for information */
   blob_buf_init(&bb_t, 0);
   blobmsg_add_string(&bb_t,"Server reply - Request has been proceeded: ", msg);
   ubus_send_reply(ctx, req, bb_t.head);
 
   /* -----  reply results to the caller ----- */
   ubus_defer_request(ctx, req, &req_data);
   ubus_complete_deferred_request(ctx, req, UBUS_STATUS_OK);

   return 0;
}

/**
 * \brief Callback function for ubus ifdown method handling
 */
int ifdown_handler( struct ubus_context *ctx, struct ubus_object *obj,
			  struct ubus_request_data *req, const char *method,
			  struct blob_attr *msg_t )
{
   int rc = 0;
   char msg[64] = {0};
   int tty;
   char pty[16] = {0};
   char *arr = NULL;   
   struct sbuf s;
   
   /* for parsed attr */
   struct blob_attr *tb[__IFDOWN_MAX];
 
   /* Parse blob_msg from the caller to request policy */
   blobmsg_parse(ifdown_policy, ARRAY_SIZE(ifdown_policy), tb, blob_data(msg_t), blob_len(msg_t));

   const char *ifname = blobmsg_data(tb[IFDOWN_IFNAME]);
   tty = blobmsg_get_u32(tb[IFDOWN_TTY]);

   if(!ifquery(ifname)) return 0;
   if(tty<0) {
	  strncpy_t(pty, sizeof(pty), "/dev/null", 9); 
   } else if(asprintf(&arr, "%d", (u_int32_t)tty) != -1) {
	  strncpy_t(pty, sizeof(pty), "/dev/pts/", 9); 
      strcat(pty, arr);
   } else {
      printf("Helper error: unable to get the tty: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
   }
   
   sbuf_init(&s);   
   sbuf_concat(&s, 5, "setsid sh -c '/sbin/ifdown ", ifname, " <> ", pty, " >&0 2>&1'");
   rc = system(s.buf);
   
   sbuf_init(&s);   
   sbuf_concat(&s, 3, "setsid sh -c '/bin/echo \"           ------------            \" <> ", pty, " >&0 2>&1'");
   rc = system(s.buf);
   
   sbuf_free(&s);
   
   strncpy_t(msg, sizeof(msg), "ifdown ", 7);
   strcat(msg, ifname);
 
   /* send a reply msg to the caller for information */
   blob_buf_init(&bb_t, 0);
   blobmsg_add_string(&bb_t,"Server reply - Request has been proceeded: ", msg);
   ubus_send_reply(ctx, req, bb_t.head);
 
   /* 	-----  reply results to the caller ----- */
   ubus_defer_request(ctx, req, &req_data);
   ubus_complete_deferred_request(ctx, req, UBUS_STATUS_OK);
   
   return rc;
}

/**
 * \brief Callback function for ubus ifup method handling
 */
int ifup_handler( struct ubus_context *ctx, struct ubus_object *obj,
			  struct ubus_request_data *req, const char *method,
			  struct blob_attr *msg_t )
{
   int rc = 0;
   char msg[64] = {0};
   int tty;
   char pty[16] = {0};
   char *arr = NULL;   
   struct sbuf s;
   
   /* for parsed attr */
   struct blob_attr *tb[__IFUP_MAX];
 
   /* Parse blob_msg from the caller to request policy */
   blobmsg_parse(ifup_policy, ARRAY_SIZE(ifup_policy), tb, blob_data(msg_t), blob_len(msg_t));

   const char *ifname = blobmsg_data(tb[IFUP_IFNAME]);
   tty = blobmsg_get_u32(tb[IFUP_TTY]);

   if(ifquery(ifname)) return 0;
   if(tty<0) {
	  strncpy_t(pty, sizeof(pty), "/dev/null", 9); 
   } else if(asprintf(&arr, "%d", (u_int32_t)tty) != -1) {
	  strncpy_t(pty, sizeof(pty), "/dev/pts/", 9); 
      strcat(pty, arr);
   } else {
      printf("Helper error: unable to get the tty: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
   }
   
   sbuf_init(&s);   
   sbuf_concat(&s, 5, "setsid sh -c '/sbin/ifup ", ifname, " <> ", pty, " >&0 2>&1'");
   rc = system(s.buf);
   
   sbuf_init(&s);   
   sbuf_concat(&s, 3, "setsid sh -c '/bin/echo \"           ------------            \" <> ", pty, " >&0 2>&1'");
   rc = system(s.buf);
   
   sbuf_free(&s);
    
   strncpy_t(msg, sizeof(msg), "ifup ", 5);
   strcat(msg, ifname);
 
   /* send a reply msg to the caller for information */
   blob_buf_init(&bb_t, 0);
   blobmsg_add_string(&bb_t,"Server reply - Request has been proceeded: ", msg);
   ubus_send_reply(ctx, req, bb_t.head);
 
   /* 	-----  reply results to the caller ----- */
   ubus_defer_request(ctx, req, &req_data);
   ubus_complete_deferred_request(ctx, req, UBUS_STATUS_OK);
   
   return rc;
}

/**
 * \brief Callback function for ubus ipaddr_flush method handling
 */
int ipaddr_flush_handler( struct ubus_context *ctx, struct ubus_object *obj,
			  struct ubus_request_data *req, const char *method,
			  struct blob_attr *msg_t )
{
   /* for parsed attr */
   struct blob_attr *tb[__IPADDR_FLUSH_MAX];
   sbuf_t wired, wireless;
   
   sbuf_init(&wired);
   sbuf_init(&wireless);
   get_interfaces(&wired, &wireless);
 
   /* Parse blob_msg from the caller to request policy */
   blobmsg_parse(ipaddr_flush_policy, ARRAY_SIZE(ipaddr_flush_policy), tb, blob_data(msg_t), blob_len(msg_t));
   
   const char *ifname = blobmsg_data(tb[IPADDR_FLUSH_IFNAME]);
   
   // For those cases where the ethernet cable was unplugged, 
   // but hadn't been disconnected by software previously
   if(!strcmp(ifname, wireless.buf)) {
      if(get_interface_status(wired.buf)) {
         interface_down(wired.buf);
         usleep(2500);
         interface_up(wired.buf);
      }
   }
   else if(!strcmp(ifname, wired.buf)) {
      if(get_interface_status(wireless.buf)) {
         interface_down(wireless.buf);
         usleep(2500);
         interface_up(wireless.buf);
      }
   }
   
   sbuf_free(&wired);
   sbuf_free(&wireless);
   sleep(1);

   kill_all_processes();
   ipaddr_flush(ifname);
 
   /* 	-----  reply results to the caller ----- */
   ubus_defer_request(ctx, req, &req_data);
   ubus_complete_deferred_request(ctx, req, UBUS_STATUS_OK);

   return 0;
}

/**
 * \brief Callback function for ubus wpa_passphrase method handling
 */
int wpa_passphrase_handler( struct ubus_context *ctx, struct ubus_object *obj,
			  struct ubus_request_data *req, const char *method,
			  struct blob_attr *msg_t )
{
   int rc = 0;   
   struct sbuf s;
   
   /* for parsed attr */
   struct blob_attr *tb[__WPA_PASSPHRASE_MAX];
 
   /* Parse blob_msg from the caller to request policy */
   blobmsg_parse(wpa_passphrase_policy, ARRAY_SIZE(wpa_passphrase_policy), tb, blob_data(msg_t), blob_len(msg_t));   

   const char *essid = blobmsg_data(tb[WPA_PASSPHRASE_ESSID]);
   const char *passwd = blobmsg_data(tb[WPA_PASSPHRASE_PASSWD]);
   const char *filename = blobmsg_data(tb[WPA_PASSPHRASE_FILENAME]);
   
   if(*passwd != 0) {
      
      sbuf_init(&s);      
      sbuf_concat(&s, 7, "/usr/bin/wpa_passphrase \"", essid, "\" \"", passwd, 
                         "\"  | sudo tee -a \"", filename, "\" > /dev/null");      
      rc = system(s.buf);
      
   } else {
   
      FILE *fp;   
      fp = fopen(filename, "w");
      if(!fp) {
         printf("Cannot open the \"%s\" config file.\n", filename);
         exit(EXIT_FAILURE);
      }
      
      fprintf( fp, "network={\n\tssid=\"%s\"\n\tkey_mgmt=NONE\n}", essid);
      fclose(fp);
   }
   
   if(access(filename, F_OK) == 0) {
      sbuf_init(&s);
      sbuf_concat(&s, 3, "chmod -R 644 \"", filename, "\" > /dev/null");
   }
   
   sbuf_free(&s);
    
   /* send a reply msg to the caller for information */
   blob_buf_init(&bb_t, 0);
   blobmsg_add_string(&bb_t,"Server reply - Request has been proceeded: ", "wpa_passphrase");
   ubus_send_reply(ctx, req, bb_t.head);
 
   /* 	-----  reply results to the caller ----- */
   ubus_defer_request(ctx, req, &req_data);
   ubus_complete_deferred_request(ctx, req, UBUS_STATUS_OK);
   
   return rc;
}

/**
 * \brief Callback function for ubus wpa_supplicant method handling
 */
int wpa_supplicant_handler( struct ubus_context *ctx, struct ubus_object *obj,
			  struct ubus_request_data *req, const char *method,
			  struct blob_attr *msg_t )
{
   int rc = 0;
   int tty;
   char pty[16] = {0};
   char *arr = NULL;      
   struct sbuf s;
   
   /* for parsed attr */
   struct blob_attr *tb[__WPA_SUPPLICANT_MAX];
 
   /* Parse blob_msg from the caller to request policy */
   blobmsg_parse(wpa_supplicant_policy, ARRAY_SIZE(wpa_supplicant_policy), tb, blob_data(msg_t), blob_len(msg_t));   
   
   tty = blobmsg_get_u32(tb[WPA_SUPPLICANT_TTY]);
   const char *ifname = blobmsg_data(tb[WPA_SUPPLICANT_IFNAME]);
   const char *filename = blobmsg_data(tb[WPA_SUPPLICANT_FILENAME]);
   
   if(tty<0) {
	  strncpy_t(pty, sizeof(pty), "/dev/null", 9);
   } else if(asprintf(&arr, "%d", (u_int32_t)tty) != -1) {
	  strncpy_t(pty, sizeof(pty), "/dev/pts/", 9); 
      strcat(pty, arr);
   } else {
      printf("Helper error: unable to get the tty: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
   }
   
   sbuf_init(&s);
   sbuf_concat(&s, 7, "setsid sh -c '/sbin/wpa_supplicant -B -i\"", ifname, 
                        "\" -c\"", filename, "\" <> ", pty, " >&0 2>&1'");
   rc = system(s.buf);
   
   if(access(filename, F_OK) == 0) {	  
	  // resort to 'strdup' because filename is  of type 'const char *' 
	  // and dirname() discards ‘const’ qualifier from pointer target type
	  // In doing so, filename preserves its value
	  char *dname = dirname(strdup(filename));	  
	  if(strcmp(dname, WIFI_DIR)) {
		 unlink(filename);
         if(access(filename, F_OK) != -1) {
            printf("Error deleting %s file.\n", filename);
         }
      }
      free(dname);
   }
   
   sbuf_free(&s);
 
   /* send a reply msg to the caller for information */
   blob_buf_init(&bb_t, 0);
   blobmsg_add_string(&bb_t,"Server reply - Request has been proceeded: ", "wpa_supplicant");
   ubus_send_reply(ctx, req, bb_t.head);
 
   /* 	-----  reply results to the caller ----- */
   ubus_defer_request(ctx, req, &req_data);
   ubus_complete_deferred_request(ctx, req, UBUS_STATUS_OK);
   
   return rc;
}

/**
 * \brief Callback function for ubus uninstall method handling
 */
int uninstall_handler( struct ubus_context *ctx, struct ubus_object *obj,
			  struct ubus_request_data *req, const char *method,
			  struct blob_attr *msg_t )
{
   char msg[64] = {0};
   char *aux = NULL;

   /* for parsed attr */
   struct blob_attr *tb[__UNINSTALL_MAX]; 
 
   /* Parse blob_msg from the caller to request policy */
   blobmsg_parse(uninstall_policy, ARRAY_SIZE(uninstall_policy), tb, blob_data(msg_t), blob_len(msg_t));

   const char *filename = blobmsg_data(tb[UNINSTALL_FILENAME]);
   aux = dirname(strdup(filename));

   if(!strcmp(aux, WIFI_DIR)) {         
      unlink(filename);
      if(access(filename, F_OK) != -1) {
         log_error("%s: Error deleting %s file.\n", progname, filename);
      }
   }
    
   free(aux);
    
   strncpy_t(msg, sizeof(msg), "uninstall ", 10);
   strcat(msg, filename);
 
   /* send a reply msg to the caller for information */
   blob_buf_init(&bb_t, 0);
   blobmsg_add_string(&bb_t,"Server reply - Request has been proceeded: ", msg);
   ubus_send_reply(ctx, req, bb_t.head);
 
   /* 	-----  reply results to the caller ----- */
   ubus_defer_request(ctx, req, &req_data);
   ubus_complete_deferred_request(ctx, req, UBUS_STATUS_OK);

   return 0;
}

/**
 * \brief Callback function for ubus scan_active_wifis method handling
 */
int scan_active_wifis_handler( struct ubus_context *ctx, struct ubus_object *obj,
			  struct ubus_request_data *req, const char *method,
			  struct blob_attr *msg_t )
{
   int rc = 0;
   struct sbuf s __cleanbuf__;

   /* for parsed attr */
   struct blob_attr *tb[__SCAN_ACTIVE_WIFIS_MAX]; 
 
   /* Parse blob_msg from the caller to request policy */
   blobmsg_parse(scan_active_wifis_policy, ARRAY_SIZE(scan_active_wifis_policy), tb, blob_data(msg_t), blob_len(msg_t));
   sbuf_init(&s);

   rc = scan_active_wifis(blobmsg_data(tb[SCAN_ACTIVE_WIFIS_IFNAME]), &s);
 
   /* send a reply msg to the caller for information */
   blob_buf_init(&bb_t, 0);
   blobmsg_add_string(&bb_t,"Server reply - Request has been proceeded: ", s.buf);
   ubus_send_reply(ctx, req, bb_t.head);

   sbuf_free(&s);
 
   /* 	-----  reply results to the caller ----- */
   ubus_defer_request(ctx, req, &req_data);
   ubus_complete_deferred_request(ctx, req, UBUS_STATUS_OK);

   return rc;
}
