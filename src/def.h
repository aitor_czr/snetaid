 /*
  * def.h
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */
   

#ifndef __DEF_H__
#define __DEF_H__

#ifndef _BSD_SOURCE
#define _DEFAULT_SOURCE 1
#define _BSD_SOURCE
#endif

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200809L
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <unistd.h>
#include <syslog.h>
#include <stdbool.h>

#include <simple-netaid/sbuf.h>

#define NORMAL_COLOR  "\x1B[0m"
#define RED           "\x1B[31m"
#define GREEN         "\x1B[32m"
#define YELLOW        "\x1B[33m"
#define BLUE          "\x1B[34m"
#define CYAN          "\x1B[36m"

#define WHERESTR "%05d: [%16s:%04u] %s: "
#define WHEREARG (int)getpid(), __FILE__, __LINE__, __func__

#define LOGLEVEL_DEBUG     2
#define LOGLEVEL_INFO      1
#define LOGLEVEL_NONE      0
#define LOGLEVEL_WARN      2
#define LOGLEVEL_ERROR     1

extern int DEBUG;
extern int _DEBUG_MESSAGES,
           _INFO_MESSAGES,
           _WARN_MESSAGES,
           _ERROR_MESSAGES; 

#define log_debug( format, ... ) \
   do { \
      if( _DEBUG_MESSAGES ) { \
         if( DEBUG ) { \
            fprintf(stderr, BLUE WHERESTR CYAN "\n[DEBUG] " NORMAL_COLOR format, WHEREARG, __VA_ARGS__); fflush(stderr);\
         } \
         else { \
            syslog( LOG_DAEMON | LOG_DEBUG, format, __VA_ARGS__ ); \
         } \
      } \
   } while(0)
   
#define log_info( format, ... ) \
   do { \
      if( _INFO_MESSAGES ) { \
         if( DEBUG ) { \
            fprintf(stderr, BLUE WHERESTR GREEN "\n[INFO] " format, WHEREARG, __VA_ARGS__); fflush(stderr);\
         } \
         else { \
            syslog( LOG_DAEMON | LOG_INFO, format, __VA_ARGS__ ); \
         } \
      } \
   } while(0)

#define log_warn( format, ... ) \
   do { \
      if( _WARN_MESSAGES ) { \
         if( DEBUG ) { \
            fprintf(stderr, BLUE WHERESTR YELLOW "\n[WARN] " NORMAL_COLOR format, WHEREARG, __VA_ARGS__); fflush(stderr);\
         } \
         else { \
            syslog( LOG_DAEMON | LOG_WARNING, format, __VA_ARGS__ ); \
         } \
      } \
   } while(0)
   
#define log_error( format, ... ) \
   do { \
      if( _ERROR_MESSAGES ) { \
         if( DEBUG ) { \
            fprintf(stderr, BLUE WHERESTR RED "\n[ERROR] " NORMAL_COLOR format, WHEREARG, __VA_ARGS__); fflush(stderr);\
         } \
         else { \
            syslog( LOG_DAEMON | LOG_ERR, format, __VA_ARGS__ ); \
         } \
      } \
   } while(0)

/*-------------------------- Globals --------------------------------*/
extern const char *progname, *devpts;
extern int pid_fd, nlsock_fd, int_val;
extern char arg0[512];
extern pid_t pid;
extern u_int32_t m_pid;
extern u_int32_t m_tty;

extern const char *ubus_socket;

#include <ubus/libubus.h>

extern const struct blobmsg_policy disconnect_policy[];
extern const struct blobmsg_policy umount_policy[];
extern const struct blobmsg_policy remove_policy[];

extern const struct ubus_method netaid_methods[];
extern struct ubus_object_type netaid_obj_type;
extern struct ubus_object netaid_obj;
extern struct ubus_context *ctx;

#define SOCKET_NAME "/tmp/snetaid.socket"
#define DEFAULT_CONFIG_FILE "/etc/simple-netaid/snetaid.conf"
#define WIFI_DIR "/etc/network/wifi"

/*--------------------- Internationalization --------------------------------*/
#include <locale.h>
#include <libintl.h>
#define _(StRiNg)  dgettext("libnetaid",StRiNg)

#endif // __DEF_H__

