 /*
  * sigact.c
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <ftw.h>          /*  nftw  */
#include <sys/un.h>      /*  UNIX socket  */
#include <sys/types.h>  
#include <sys/socket.h>
#include <assert.h>
#include <inttypes.h>
#include <ctype.h>       /* ???   */

#include <simple-netaid/sbuf.h>
#include <simple-netaid/helpers.h>
#include <simple-netaid/interfaces.h>
#include <simple-netaid/ipaddr.h>
#include <simple-netaid/wireless.h>
#include <simple-netaid/iproute.h>
#include <simple-netaid/ethlink.h>

#include "sigact.h"
#include "def.h"
#include "config.h"
#include "proc.h"
#include "util.h"

#define MAX_SIZE 1024


/* This flag controls termination of the main loop. */
volatile sig_atomic_t keep_going = 1;


static
int wake_up()
{
   char wlan[32]={0};
   sbuf_t wired, wireless;
   bool eth_ok = false;
   bool wlan_ok = false;
   
   sbuf_init(&wired);
   sbuf_init(&wireless);
   get_interfaces(&wired, &wireless);
   
   interface_up(wired.buf);
   
   if(get_interface_status(wired.buf) && ethlink(wired.buf))
      eth_ok = true;
   
   if(eth_ok && *iproute() == 0) {
      char ifup_cmd[128]={0};
      strncpy_t(ifup_cmd, sizeof(ifup_cmd), "/sbin/ifup ", 11);
      strcat(ifup_cmd, wired.buf); 
      system(ifup_cmd);
      /*
      if(*iproute() == 0) {
         char ifdown_cmd[128]={0};
         strncpy_t(ifdown_cmd, 128, "/sbin/ifdown ", 13);
         strcat(ifdown_cmd, wired.buf); 
         system(ifdown_cmd);
         system(ifup_cmd);
      }
      * */
   }
   
   if(wireless.len>2)
      wlan_ok = true;
   
   if(wlan_ok) {
      strncpy_t(wlan, sizeof(wlan), wireless.buf, wireless.len); 
	  interface_up(wlan);
   }
   
   sbuf_free(&wired);
   sbuf_free(&wireless);
     
   if(keep_going == 0)
      return 1;

   if(*iproute() == 0) {
      if(wlan_ok && get_interface_status(wlan)) {
	     automatically_connect(wlan);
	     keep_going = 0;
	  }
	     
      if(*iproute() == 0 && keep_going)
         alarm(1);
   
   } else if(!wlan_ok) {
	  // Still not detected...?
	  // Give a last chance for USB devices five seconds later 
      keep_going = 0;
      alarm(5);
   }   

   return 0;
}

void sig_handler(int signum, siginfo_t *info, void *extra)
{ 
   switch(signum) {
      
      case SIGALRM:
         //signal(SIGALRM, SIG_IGN);   
         wake_up();   
         sigaction_init();
         break;
        
      case SIGHUP:
         break;
        
      case SIGUSR1:
         break;
         
      case SIGTERM:
      case SIGINT:
      case SIGQUIT:
         {			 
			// rt_netlink monitoring part
            if( access( m_config->nl_monitor_path, F_OK ) == 0 ) {
               char *temp = rindex(m_config->nl_monitor_path, '/') + 1;	          	                   
               unlink(m_config->nl_monitor_path);
               // Remove the monitor directory only if it's a subdir of tmp
	           // We do not want to rely on the use of 'tmpreaper', similar 
	           // to 'tmpwatch' in redhat and derivatives, aimed to cleanup 
	           // empty directories in /tmp
               if(strcmp(str_slice(m_config->nl_monitor_path, 0, 5), "/tmp/") == 0)
                  rmdir(str_slice(m_config->nl_monitor_path, 0, strlen(m_config->nl_monitor_path) - strlen(temp) - 1));
            }
                
            // Ubus loop part
            if( access( m_config->pidfile_path, F_OK ) == 0 ) {
               uloop_done();
               ubus_free(ctx);
               if(pid_fd != -1) {
                  int rc = lockf(pid_fd, F_ULOCK, 0);
                  if(rc != 0) exit(EXIT_FAILURE);
                  close(pid_fd);
               }
               unlink(m_config->pidfile_path);
            }
                        
            // kill the child process
            kill_child(SIGTERM);
         }
         exit(EXIT_SUCCESS);  /* exit gracefully. */
         break;
         
      default:
         break;
   }
}

void sigaction_init(void)
{
   struct sigaction sa;
   memset (&sa, 0, sizeof(sa));
     
   /*---------------- initialize signal handling ------------ */
   //sa.sa_handler = sig_handler;   
   sa.sa_sigaction = &sig_handler;
   sa.sa_flags =  SA_RESETHAND | SA_RESTART | SA_NODEFER;  // SA_SIGINFO; 
   sigemptyset(&sa.sa_mask);
   sigaction(SIGALRM, &sa, 0);
   sigaction(SIGHUP,  &sa, 0);
   sigaction(SIGUSR1, &sa, 0);
   sigaction(SIGTERM, &sa, 0);
   sigaction(SIGINT,  &sa, 0);
   sigaction(SIGQUIT, &sa, 0);
}
