 /*
  * policy.h
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */
   
#ifndef __POLICY_H__
#define __POLICY_H__

#include <stdio.h>
#include <stdlib.h>

#include "def.h"
			  
int interface_down_handler( struct ubus_context*, struct ubus_object*,
			  struct ubus_request_data*, const char*,
			  struct blob_attr* );			  
			  
int interface_up_handler( struct ubus_context*, struct ubus_object*,
			  struct ubus_request_data*, const char*,
			  struct blob_attr* );				  
			  			  
int ifup_handler( struct ubus_context*, struct ubus_object*,
			  struct ubus_request_data*, const char*,
			  struct blob_attr* );
			  
int ifdown_handler( struct ubus_context*, struct ubus_object*,
			  struct ubus_request_data*, const char*,
			  struct blob_attr* );
			  			  
int ipaddr_flush_handler( struct ubus_context*, struct ubus_object*,
			  struct ubus_request_data*, const char*,
			  struct blob_attr* );

int wpa_passphrase_handler( struct ubus_context*, struct ubus_object*,
			  struct ubus_request_data*, const char*,
			  struct blob_attr* );

int wpa_supplicant_handler( struct ubus_context*, struct ubus_object*,
			  struct ubus_request_data*, const char*,
			  struct blob_attr* );
			  
int uninstall_handler( struct ubus_context*, struct ubus_object*,
			  struct ubus_request_data*, const char*,
			  struct blob_attr* );

int scan_active_wifis_handler( struct ubus_context*, struct ubus_object*,
			  struct ubus_request_data*, const char*,
			  struct blob_attr* );
			  
#endif  // __POLICY_H__
