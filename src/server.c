 /*
  * main.c - Monitor-netlink
  * 
  * This file makes use of the following monitor-netlink example:
  * 
  * https://github.com/lihongguang/monitor-netlink
  * 
  * All modifications to the original source file are:
  * Copyright (C) 2021 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */
   
  
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <libgen.h>   // dirname() and basename()
#include <time.h>
#include <signal.h>
#include <memory.h>
#include <net/if.h>
#include <asm/types.h>
#include <arpa/inet.h>
#include <sys/un.h>      /*  UNIX socket  */
#include <sys/types.h>  
#include <sys/socket.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <netinet/in.h>
#include <assert.h>
#include <inttypes.h>
#include <sys/wait.h>
#include <ctype.h>      // isdigit()

#include <sys/wait.h>

   
#define log_error_async_safe( msg ) \
   do { \
       write( STDERR_FILENO, "[snetaid error async] " msg, strlen( "[snetaid error async] " msg ) ); \
   } while(0)

#include <simple-netaid/sbuf.h>
#include <simple-netaid/iproute.h>
#include <simple-netaid/interfaces.h>
#include <simple-netaid/helpers.h>
#include <simple-netaid/ipaddr.h>
#include <simple-netaid/wireless.h>
#include <simple-netaid/ethlink.h>
#include <pstat/libpstat.h>

#include "ini.h"
#include "def.h"
#include "config.h"
#include "util.h"
#include "sigact.h"
#include "policy.h"
#include "proc.h"

#define MAX_SIZE 1024 

int DEBUG = 1;

int _DEBUG_MESSAGES = 0;
int _INFO_MESSAGES = 0;
int _WARN_MESSAGES = 1;
int _ERROR_MESSAGES = 1;

// Global
const char *progname;

config_t *m_config = NULL;
const char *libpath, *devpts;
char arg0[512] = {0};
int pid_fd = -1;      // lock file
pid_t pid = 0;
u_int32_t m_pid = 0;
u_int32_t m_tty = 0;

struct ubus_context *ctx;
const char *ubus_socket="/var/run/ubus/ubus.sock";

static int idx = 0;
static int idx_flags = 0;
static bool ethconn_ok = false;

static
void wait_on_ifupdown(int time)
{
   FILE *fp = NULL;
   char ls[128] = {0};
   
   fp = popen("ls /var/run/network | grep .pid", "r");
    if(!fp) {
       printf("Sorry, there was an error opening the pipe\n");
       exit(EXIT_FAILURE);
    }
   
    usleep(time);
    if(fgets(ls, 128, fp)!=NULL) {
	   char path[128] = {0};
	   ls[strcspn(ls, "\n")] = 0;
	   strncpy_t(path, sizeof(path), "/var/run/network/", 17);
	   strcat(path, ls);
       while(access(path, F_OK ) != 0) sleep(1);
    }
    pclose(fp);
}

static
int touch(const char *filename)
{
   int fd = open(filename, O_CREAT | S_IRUSR | S_IWUSR);
   if (fd == -1) {
      perror("Unable to touch file");
      return 1;
   }
   close(fd);
   return 0;
}

void log_netlink_event(char *buf);

/* Message structure. */
struct message
{
   int key;
   const char *str;
};

/* Socket interface to kernel */
struct nlsock
{
   int sock;
   int seq;
   struct sockaddr_nl snl;
   const char *name;
} netlink = { -1, 0, {0}, "netlink-listen"}; /* kernel messages */

#include "prefix.h"

// set debug level, by setting for info and debug messages
static
void set_debug_level(int d)
{   
   if(d >= 1) {
      _INFO_MESSAGES = 1;
   }
   if(d >= 2) {
      _DEBUG_MESSAGES = 1;
   }
}

// set error level, by setting for warning and error messages
static
void set_error_level(int e)
{ 
   if(e >= 1) {
      _ERROR_MESSAGES = 1;
   }
   if(e >= 2) {
      _WARN_MESSAGES = 1;
   }
}

static
inline char *if_index2name(int index, char *name)
{
   char *if_name = if_indextoname(index, name);
   if (name == NULL && errno == ENXIO)
      return "";
   else
      return if_name;
}

static
inline unsigned int if_name2index(char *name)
{
   return if_nametoindex(name);
}

/* Message lookup function. */
static
const char *lookup(const struct message *mes, int key)
{
   const struct message *pnt;

   for (pnt = mes; pnt->str; pnt++)
      if (pnt->key == key)
         return pnt->str;

   return "unknown";
}

/* Wrapper around strerror to handle case where it returns NULL. */
const char *safe_strerror(int errnum)
{
   const char *s = strerror(errnum);
   return (s != NULL) ? s : "Unknown error";
}

/* Make socket for Linux netlink interface. */
static
int netlink_socket (struct nlsock *nl, unsigned long groups)
{
   int ret =0;
   struct sockaddr_nl snl;
   int sock;
   int namelen;

   sock = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
   if (sock < 0) {
     log_error("Can't open %s socket: %s\n", nl->name, safe_strerror(errno));
      return -1;
   }

   memset (&snl, 0, sizeof(snl));
   snl.nl_family = AF_NETLINK;
   snl.nl_groups = groups;

   /* Bind the socket to the netlink structure for anything. */
   ret = bind(sock, (struct sockaddr *)&snl, sizeof(snl));
   if (ret < 0) {
     log_error("Can't bind %s socket to group 0x%x: %s\n",
           nl->name, snl.nl_groups, safe_strerror(errno));
      close (sock);
      return -1;
   }

   /* multiple netlink sockets will have different nl_pid */
   namelen = sizeof(snl);
   ret = getsockname(sock, (struct sockaddr *)&snl, (socklen_t *)&namelen);
   if (ret < 0 || namelen != sizeof(snl)) {
     log_error("Can't get %s socket name: %s\n", nl->name, safe_strerror(errno));
      close (sock);
      return -1;
   }

   nl->snl = snl;
   nl->sock = sock;
   return ret;
}

#ifndef HAVE_NETLINK
#define HAVE_NETLINK
/* Receive buffer size for netlink socket */
u_int32_t nl_rcvbufsize = 4194304;
#endif /* HAVE_NETLINK */

#ifndef SO_RCVBUFFORCE
#define SO_RCVBUFFORCE  (33)
#endif

static
int netlink_recvbuf(struct nlsock *nl, uint32_t newsize)
{
   u_int32_t oldsize;
   socklen_t newlen = sizeof(newsize);
   socklen_t oldlen = sizeof(oldsize);
   int rc __attribute__((unused)) = 0;
   char nl_tmp[MAX_SIZE] = {0};

   rc = getsockopt(nl->sock, SOL_SOCKET, SO_RCVBUF, &oldsize, &oldlen);
   if (rc < 0) {
     log_error("Can't get %s receive buffer size: %s\n", nl->name, safe_strerror(errno));
      return -1;
   }

   /* Try force option (linux >= 2.6.14) and fall back to normal set */
   rc = setsockopt(nl->sock, SOL_SOCKET, SO_RCVBUFFORCE, &nl_rcvbufsize,
         sizeof(nl_rcvbufsize));
   if (rc < 0) {
      rc = setsockopt(nl->sock, SOL_SOCKET, SO_RCVBUF, &nl_rcvbufsize,
            sizeof(nl_rcvbufsize));
      if (rc < 0) {
        log_error("Can't set %s receive buffer size: %s\n", nl->name, safe_strerror(errno));
         return -1;
      }
   }

   rc = getsockopt(nl->sock, SOL_SOCKET, SO_RCVBUF, &newsize, &newlen);
   if (rc < 0) {
     log_error("Can't get %s receive buffer size: %s\n", nl->name, safe_strerror(errno));
      return -1;
   }   
   
   memset(&nl_tmp, 0, MAX_SIZE);
   //rc = snprintf(nl_tmp, sizeof(nl_tmp), "Setting netlink socket receive buffer size: %u -> %u", oldsize, newsize);
   log_netlink_event(nl_tmp);
  
   return 0;
}

void kernel_init (void)
{
   unsigned long groups;

   groups = RTMGRP_LINK | RTMGRP_IPV4_ROUTE | RTMGRP_IPV4_IFADDR |
      RTMGRP_IPV6_ROUTE | RTMGRP_IPV6_IFADDR;
   netlink_socket (&netlink, groups);

   /* Register kernel socket. */
   if (netlink.sock > 0) {
#if 0
      /* Only want non-blocking on the netlink event socket */
      if (fcntl (netlink.sock, F_SETFL, O_NONBLOCK) < 0) {
         log_error("%s: Can't set %s socket flags: %s\n", progname, nl->name, safe_strerror(errno));
      }
#endif
      /* Set receive buffer size if it's set from command line */
      if (nl_rcvbufsize)
         netlink_recvbuf (&netlink, nl_rcvbufsize);
   }
}

static
int is_valid_kernel_table(u_int32_t table_id)
{
   if ((table_id == RT_TABLE_MAIN) ||
      (table_id == RT_TABLE_LOCAL) ||
      (table_id == RT_TABLE_COMPAT) ||
      (table_id > RT_TABLE_UNSPEC))
      return 1;
   else
      return 0;
}

/* Utility function for parse rtattr. */
static
void netlink_parse_rtattr (struct rtattr **tb, int max,
   struct rtattr *rta, int len)
{
   while (RTA_OK (rta, len)) {
      if (rta->rta_type <= max)
         tb[rta->rta_type] = rta;
      rta = RTA_NEXT (rta, len);
   }
}

/* Routing information change from the kernel. */
static
int netlink_route_change(struct sockaddr_nl *snl, struct nlmsghdr *h)
{
   int len;
   struct rtmsg *rtm;
   struct rtattr *tb[RTA_MAX + 1];
   char anyaddr[16] = {0};
   //char straddr[INET6_ADDRSTRLEN];
   char if_name[IFNAMSIZ];
   int rc __attribute__((unused)) = 0;
   char nl_msg[MAX_SIZE] = {0};
   char nl_tmp[MAX_SIZE];

   int index;
   int table;
   int metric __attribute((unused));

   void *dest;
   void *gate;
   void *src __attribute((unused));

   rtm = NLMSG_DATA(h);

   if (h->nlmsg_type != RTM_NEWROUTE && h->nlmsg_type != RTM_DELROUTE) {
      /* If this is not route add/delete message print warning. */
     memset(&nl_tmp, 0, MAX_SIZE);
     rc = snprintf(nl_tmp, sizeof(nl_tmp), "Kernel message: %d\n", h->nlmsg_type);
     log_netlink_event(nl_tmp);
      return 0;
   }

   if (rtm->rtm_flags & RTM_F_CLONED) {
      log_netlink_event("This route is cloned from another route!\n");
      return 0;
   }
 
   rc = snprintf(nl_msg, sizeof(nl_msg), 
          "nlmsg: %s, family: %s, rtable: %d-%s, rtype: %d-%s, rtproto: %d-%s\n",
          lookup(nlmsg_str, h->nlmsg_type),
          rtm->rtm_family == AF_INET ? "ipv4" : "ipv6",
          rtm->rtm_table, lookup(rtable_str, rtm->rtm_table),
          rtm->rtm_type, lookup(rtype_str, rtm->rtm_type),
          rtm->rtm_protocol, lookup(rtproto_str, rtm->rtm_protocol));

   table = rtm->rtm_table;
   if (!is_valid_kernel_table(table)) {
      memset(&nl_tmp, 0, MAX_SIZE);
      rc = snprintf(nl_tmp, sizeof(nl_tmp), "invalid kernel table: %d\n", table);
      log_netlink_event(nl_tmp);
      return 0;
   }

   len = h->nlmsg_len - NLMSG_LENGTH(sizeof(struct rtmsg));
   if (len < 0) {
      log_netlink_event("netlink msg length error!\n");
      return -1;
   }

   memset(tb, 0, sizeof tb);
   netlink_parse_rtattr(tb, RTA_MAX, RTM_RTA(rtm), len);

   if (rtm->rtm_src_len != 0) {
      log_netlink_event("netlink_route_change(): no src len\n");
      return 0;
   }

   index = 0;
   metric = 0;
   dest = NULL;
   gate = NULL;
   src = NULL;

   if (tb[RTA_OIF])
      index = *(int *)RTA_DATA(tb[RTA_OIF]);

   if (tb[RTA_DST])
      dest = RTA_DATA(tb[RTA_DST]);
   else
      dest = anyaddr;

   if (tb[RTA_GATEWAY])
      gate = RTA_DATA(tb[RTA_GATEWAY]);

   if (tb[RTA_PREFSRC])
      src = RTA_DATA(tb[RTA_PREFSRC]);

   if (h->nlmsg_type == RTM_NEWROUTE && tb[RTA_PRIORITY])
      metric = *(int *)RTA_DATA(tb[RTA_PRIORITY]);

   if (rtm->rtm_family == AF_INET) {
      struct prefix_ipv4 p;
      p.family = AF_INET;
      memcpy (&p.prefix, dest, 4);
      p.prefixlen = rtm->rtm_dst_len;

      //inet_ntop(p->family, &p->u.prefix, straddr, INET6_ADDRSTRLEN);

      memset(&nl_tmp, 0, MAX_SIZE);
      if (h->nlmsg_type == RTM_NEWROUTE) {
        rc = snprintf(nl_tmp, sizeof(nl_tmp), "\tadd route %s/%d", inet_ntoa(p.prefix), p.prefixlen);
        strcat(nl_msg, nl_tmp);
      }
      else {
        rc = snprintf(nl_tmp, sizeof(nl_tmp), "\tdel route %s/%d", inet_ntoa(p.prefix), p.prefixlen);
        strcat(nl_msg, nl_tmp);
      }
     
      memset(&nl_tmp, 0, MAX_SIZE);
      if (!tb[RTA_MULTIPATH]) {
         if (gate) {
            if (index) {
              rc = snprintf(nl_tmp, sizeof(nl_tmp), " nexthop via %s dev %s\n", inet_ntoa(*(struct in_addr *)gate), if_index2name(index, if_name));
              strcat(nl_msg, nl_tmp);
           }
            else {
              rc = snprintf(nl_tmp, sizeof(nl_tmp), " nexthop via %s\n", inet_ntoa(*(struct in_addr *)gate));
              strcat(nl_msg, nl_tmp);
           }
         } else {
            if (index) {
              rc = snprintf(nl_tmp, sizeof(nl_tmp), " dev %s\n", if_index2name(index, if_name));
              strcat(nl_msg, nl_tmp);
           }
         }
      } else {
         /* This is a multipath route */
         struct rtnexthop *rtnh = (struct rtnexthop *)RTA_DATA(tb[RTA_MULTIPATH]);

         len = RTA_PAYLOAD (tb[RTA_MULTIPATH]);
         for (;;) {
            if (len < (int)sizeof(*rtnh) || rtnh->rtnh_len > len)
               break;

            index = rtnh->rtnh_ifindex;
            gate = 0;
            if (rtnh->rtnh_len > sizeof (*rtnh)) {
               memset (tb, 0, sizeof(tb));
               netlink_parse_rtattr (tb, RTA_MAX, RTNH_DATA(rtnh),
                  rtnh->rtnh_len - sizeof(*rtnh));
               if (tb[RTA_GATEWAY])
                  gate = RTA_DATA(tb[RTA_GATEWAY]);
            }

            if (gate) {
               if (index) {
                 rc = snprintf(nl_tmp, sizeof(nl_tmp), " nexthop via %s dev %s\n", inet_ntoa(*(struct in_addr *)gate), if_index2name(index, if_name));
                 strcat(nl_msg, nl_tmp);
              }
               else {
                 rc = snprintf(nl_tmp, sizeof(nl_tmp), " nexthop via %s\n", inet_ntoa(*(struct in_addr *)gate));
                 strcat(nl_msg, nl_tmp);
              }
            } else {
               if (index) {
                 rc = snprintf(nl_tmp, sizeof(nl_tmp), " dev %s\n", if_index2name(index, if_name));
                 strcat(nl_msg, nl_tmp);
              }
            }
            len -= NLMSG_ALIGN(rtnh->rtnh_len);
            rtnh = RTNH_NEXT(rtnh);
         }
      }
   }

#ifdef HAVE_IPV6
   if (rtm->rtm_family == AF_INET6) {
      struct prefix_ipv6 p;
      char buf[BUFSIZ];

      p.family = AF_INET6;
      memcpy (&p.prefix, dest, 16);
      p.prefixlen = rtm->rtm_dst_len;
     
     memset(&nl_tmp, 0, MAX_SIZE);
      if (h->nlmsg_type == RTM_NEWROUTE) {
        rc = snprintf(nl_tmp, sizeof(nl_tmp), "\tadd route %s/%d", inet_ntop(AF_INET6, &p.prefix, buf, BUFSIZ), p.prefixlen);
        strcat(nl_msg, nl_tmp);
     }
      else {
        rc = snprintf(nl_tmp, sizeof(nl_tmp), "\tdel route %s/%d", inet_ntop(AF_INET6, &p.prefix, buf, BUFSIZ), p.prefixlen);
        strcat(nl_msg, nl_tmp);
     }

      // FIXME: add multipath process.
      if (h->nlmsg_type == RTM_NEWROUTE) {
        memset(&nl_tmp, 0, MAX_SIZE);
         if (gate) {
            if (index) {
              rc = snprintf(nl_tmp, sizeof(nl_tmp), " nexthop via %s dev %s\n", inet_ntop(AF_INET6, gate, buf, INET6_ADDRSTRLEN), if_index2name(index, if_name));
              strcat(nl_msg, nl_tmp);
           }
            else {
              rc = snprintf(nl_tmp, sizeof(nl_tmp), " nexthop via %s\n", inet_ntop(AF_INET6, gate, buf, INET6_ADDRSTRLEN));
              strcat(nl_msg, nl_tmp);
           }
         } else {
           rc = snprintf(nl_tmp, sizeof(nl_tmp), " dev %s\n", if_index2name(index, if_name));
           strcat(nl_msg, nl_tmp);
        }
      }
      else {
         if (gate) {
           memset(&nl_tmp, 0, MAX_SIZE);
           rc = snprintf(nl_tmp, sizeof(nl_tmp), " nexthop via %s\n", inet_ntop(AF_INET6, gate, buf, INET6_ADDRSTRLEN));
           strcat(nl_msg, nl_tmp);
        }
         if (index) {
           memset(&nl_tmp, 0, MAX_SIZE);
           rc = snprintf(nl_tmp, sizeof(nl_tmp), " dev %s", if_index2name(index, if_name));
           strcat(nl_msg, nl_tmp);
        }
         if (gate || index)
            strcat(nl_msg, "\n");
      }
   }
#endif /* HAVE_IPV6 */

   if(*nl_msg != 0) log_netlink_event(nl_msg);

   return 0;
}

/* Utility function to parse hardware link-layer address */
static
void netlink_interface_get_hw_addr(struct rtattr **tb,
   u_char hw_addr[], int *hw_addr_len)
{
   int i;
   int rc __attribute__((unused)) = 0;
   char nl_msg[MAX_SIZE] = {0};

   if (tb[IFLA_ADDRESS]) {
      int __hw_addr_len;

      __hw_addr_len = RTA_PAYLOAD(tb[IFLA_ADDRESS]);

      if (__hw_addr_len > IF_HWADDR_MAX) {
        rc = snprintf(nl_msg, sizeof(nl_msg), "Hardware address is too large: %d\n", __hw_addr_len);
        log_netlink_event(nl_msg);
      } else {
         *hw_addr_len = __hw_addr_len;
         memcpy(hw_addr, RTA_DATA(tb[IFLA_ADDRESS]), __hw_addr_len);

         for (i = 0; i < __hw_addr_len; i++)
            if (hw_addr[i] != 0)
               break;

         if (i == __hw_addr_len)
            *hw_addr_len = 0;
         else
            *hw_addr_len = __hw_addr_len;
      }
   }
}

static
int netlink_link_change (struct sockaddr_nl *snl, struct nlmsghdr *h)
{
   int len;
   unsigned int index;
   struct ifinfomsg *ifi;
   struct rtattr *tb[IFLA_MAX + 1];
   char *name;
   int rc __attribute__((unused)) = 0;
   char nl_msg[MAX_SIZE] = {0};
   char nl_tmp[MAX_SIZE];

   ifi = NLMSG_DATA (h);

   if (!(h->nlmsg_type == RTM_NEWLINK || h->nlmsg_type == RTM_DELLINK)) {
      /* If this is not link add/delete message so print warning. */
     log_error("netlink_link_change: wrong kernel message %d\n", h->nlmsg_type);
      return 0;
   }

   len = h->nlmsg_len - NLMSG_LENGTH(sizeof(struct ifinfomsg));
   if (len < 0)
      return -1;

   /* Looking up interface name. */
   memset (tb, 0, sizeof tb);
   netlink_parse_rtattr (tb, IFLA_MAX, IFLA_RTA(ifi), len);

#ifdef IFLA_WIRELESS
   /* check for wireless messages to ignore */
   if ((tb[IFLA_WIRELESS] != NULL) && (ifi->ifi_change == 0)) {
     memset(&nl_tmp, 0, MAX_SIZE);
     rc = snprintf(nl_tmp, sizeof(nl_tmp), "%s: ignoring IFLA_WIRELESS message\n", __func__);
     //log_netlink_event(nl_tmp);
      return 0;
   }
#endif /* IFLA_WIRELESS */

   if (tb[IFLA_IFNAME] == NULL)
      return -1;
   name = (char *)RTA_DATA(tb[IFLA_IFNAME]);

   /* Add interface. */
   if (h->nlmsg_type == RTM_NEWLINK) {
      u_char hw_addr[IF_HWADDR_MAX];
      int hw_addr_len = 0;

      index = if_name2index(name);
      if (0 == index)  

        rc = snprintf(nl_msg, sizeof(nl_tmp), "add link dev %s index %d", name, ifi->ifi_index);

      else {
        /* Interface status change. */         
        rc = snprintf(nl_msg, sizeof(nl_tmp), "update link dev %s index %d", name, ifi->ifi_index);

        if(!strcmp(name, "eth0")) {
           int res = abs(ifi->ifi_flags - idx_flags);
           idx = idx + ifi->ifi_change;
           // DEBUG
             //printf("\n---> [%d,%d] %d,%d\n", idx, res, ifi->ifi_change, ifi->ifi_flags & 0x0000fffff); 
           if(res > 1 && idx_flags > 0) {
		      idx_flags = 0;
		      if((idx == 0) && *iproute() == 0)
		         ethconn_ok = true;
		      else 
                 idx = 0;
           }
           else {
              idx_flags = ifi->ifi_flags;
              if(res == 0)
                 idx = 0;
           }
        }
      }

      netlink_interface_get_hw_addr(tb, hw_addr, &hw_addr_len);

      int i;
      for (i = 0; i < hw_addr_len; i++) {
        memset(&nl_tmp, 0, MAX_SIZE);
        rc = snprintf(nl_tmp, sizeof(nl_tmp), "%s%02x", i == 0 ? "" : ":", hw_addr[i]);
        strcat(nl_msg, nl_tmp);
      }
      memset(&nl_tmp, 0, MAX_SIZE);
      rc = snprintf(nl_tmp, sizeof(nl_tmp), " mtu %d flags %d\n", *(int *)RTA_DATA(tb[IFLA_MTU]), ifi->ifi_flags & 0x0000fffff);
      strcat(nl_msg, nl_tmp);

   } else {
      /* RTM_DELLINK. */
      index = if_name2index(name);
      memset(&nl_tmp, 0, MAX_SIZE);
      if (0 == index) {
        rc = snprintf(nl_tmp, sizeof(nl_tmp), "interface %s is deleted but can't find\n", name);
      }
      else {
        rc = snprintf(nl_tmp, sizeof(nl_tmp), "delete link dev %s index %d\n", name, ifi->ifi_index);
      }
      strcat(nl_msg, nl_tmp);
   }
   
   if(*nl_msg != 0) log_netlink_event(nl_msg);

   return 0;
}

/* Lookup interface IPv4/IPv6 address. */
static
int netlink_interface_addr(struct sockaddr_nl *snl, struct nlmsghdr *h)
{
   int len;
   struct ifaddrmsg *ifa;
   struct rtattr *tb[IFA_MAX + 1];
   char name[IFNAMSIZ];
   char *if_name;
   char buf[BUFSIZ];
   int rc __attribute__((unused)) = 0;
   char nl_msg[MAX_SIZE] = {0};
   char nl_tmp[MAX_SIZE];

   ifa = NLMSG_DATA (h);

   if (ifa->ifa_family != AF_INET
#ifdef HAVE_IPV6
   && ifa->ifa_family != AF_INET6
#endif /* HAVE_IPV6 */
   )
   return 0;

   if (h->nlmsg_type != RTM_NEWADDR && h->nlmsg_type != RTM_DELADDR)
      return 0;

   len = h->nlmsg_len - NLMSG_LENGTH(sizeof(struct ifaddrmsg));
   if (len < 0)
      return -1;

   memset (tb, 0, sizeof tb);
   netlink_parse_rtattr (tb, IFA_MAX, IFA_RTA(ifa), len);

   if_name = if_index2name(ifa->ifa_index, name);
   if (!if_name || if_name[0] == '\0') {
      log_error("netlink_interface_addr can't find interface by index %d\n", ifa->ifa_index);
      return -1;
   }

   rc = snprintf(nl_msg, sizeof(nl_tmp), "nlmsg: %s, family: %s, %s on dev %s\n",
          lookup(nlmsg_str, h->nlmsg_type), ifa->ifa_family == AF_INET ? "ipv4" : "ipv6",
          (h->nlmsg_type == RTM_NEWADDR) ? "add addr" : "del addr", if_name);

   if (tb[IFA_LOCAL]) {
     memset(&nl_tmp, 0, MAX_SIZE);
     rc = snprintf(nl_tmp, sizeof(nl_tmp), "\t  IFA_LOCAL     %s/%d\n",
         inet_ntop (ifa->ifa_family, RTA_DATA(tb[IFA_LOCAL]),
            buf, BUFSIZ), ifa->ifa_prefixlen);
      strcat(nl_msg, nl_tmp);
   }
   if (tb[IFA_ADDRESS]) {
     memset(&nl_tmp, 0, MAX_SIZE);
     rc = snprintf(nl_tmp, sizeof(nl_tmp), "\t  IFA_ADDRESS   %s/%d\n",
         inet_ntop(ifa->ifa_family, RTA_DATA (tb[IFA_ADDRESS]),
            buf, BUFSIZ), ifa->ifa_prefixlen);
      strcat(nl_msg, nl_tmp);
   }
   if (tb[IFA_BROADCAST]) {
     memset(&nl_tmp, 0, MAX_SIZE);
     rc = snprintf(nl_tmp, sizeof(nl_tmp), "\t  IFA_BROADCAST %s/%d\n",
         inet_ntop(ifa->ifa_family, RTA_DATA(tb[IFA_BROADCAST]),
            buf, BUFSIZ), ifa->ifa_prefixlen);
     strcat(nl_msg, nl_tmp);
   }
   if (tb[IFA_LABEL] && strcmp (if_name, RTA_DATA(tb[IFA_LABEL]))) {
     memset(&nl_tmp, 0, MAX_SIZE);
     rc = snprintf(nl_tmp, sizeof(nl_tmp), "\t  IFA_LABEL     %s\n", (char *)RTA_DATA(tb[IFA_LABEL]));
     strcat(nl_msg, nl_tmp);
   }

   if (tb[IFA_CACHEINFO]) {
      struct ifa_cacheinfo *ci = RTA_DATA(tb[IFA_CACHEINFO]);
      memset(&nl_tmp, 0, MAX_SIZE);
     rc = snprintf(nl_tmp, sizeof(nl_tmp), "\t  IFA_CACHEINFO pref %d, valid %d\n", ci->ifa_prefered, ci->ifa_valid);
     strcat(nl_msg, nl_tmp);
   }
   
   if(*nl_msg != 0) log_netlink_event(nl_msg);
 
   return 0;
}

static
int netlink_information_fetch(struct sockaddr_nl *snl, struct nlmsghdr *h)
{
   /* Ignore messages that aren't from the kernel */
   if (snl->nl_pid != 0) {
      log_error("Ignoring message from pid %u that isn't from the kernel!\n", snl->nl_pid);
      return 0;
   }
 
   switch (h->nlmsg_type) {
   case RTM_NEWROUTE:
      return netlink_route_change(snl, h);
      break;
   case RTM_DELROUTE:
      return netlink_route_change(snl, h);
      break;
   case RTM_NEWLINK:
      return netlink_link_change(snl, h);
      break;
   case RTM_DELLINK:
      return netlink_link_change(snl, h);
      break;
   case RTM_NEWADDR:
      return netlink_interface_addr(snl, h);
      break;
   case RTM_DELADDR:
      return netlink_interface_addr(snl, h);
      break;
   default:
      log_error("Unknown netlink nlmsg_type %d\n", h->nlmsg_type);
      break;
   }
   
   return 0;
}


#define NL_PKT_BUF_SIZE         8192

/* Hack for GNU libc version 2. */
#ifndef MSG_TRUNC
#define MSG_TRUNC      0x20
#endif /* MSG_TRUNC */

/* Receive message from netlink interface and pass those information
 * to the given function.
 */
static
int netlink_parse_info (int (*filter)(struct sockaddr_nl *, struct nlmsghdr *),
   struct nlsock *nl)
{
   int status;
   int ret = 0;
   int error;
   int rc __attribute__((unused)) = 0;
   char nl_tmp[MAX_SIZE] = {0};

   while (1) {
      char buf[NL_PKT_BUF_SIZE];
      struct iovec iov = {
         .iov_base = buf,
         .iov_len = sizeof(buf)
      };
      struct sockaddr_nl snl;
      struct msghdr msg = {
         .msg_name = (void *)&snl,
         .msg_namelen = sizeof(snl),
         .msg_iov = &iov,
         .msg_iovlen = 1
      };
      struct nlmsghdr *h;
      
      if(ethconn_ok) {
		  
         char ifdown_cmd[128]={0};
		 char ifup_cmd[128]={0};
		 sbuf_t wired, wireless;
   
         sbuf_init(&wired);
         sbuf_init(&wireless);
         get_interfaces(&wired, &wireless);
         
         if(get_interface_status(wireless.buf)) {
            interface_down(wireless.buf);
            usleep(2500);
            interface_up(wireless.buf);
         }
         sbuf_free(&wireless);
          
         ipaddr_flush(wired.buf);
         interface_down(wired.buf);
		    
         strncpy_t(ifdown_cmd, sizeof(ifdown_cmd), "/sbin/ifdown ", 13);
         strcat(ifdown_cmd, wired.buf); 
         system(ifdown_cmd);
         system(ifup_cmd);
            
         interface_up(wired.buf);
		 sleep(1);
            
		 strncpy_t(ifup_cmd, sizeof(ifup_cmd), "/sbin/ifup ", 11);
		 strcat(ifup_cmd, wired.buf);
         system(ifup_cmd);

         sbuf_free(&wired); 
          
         ethconn_ok = false;
      }
      
      status = recvmsg(nl->sock, &msg, 0);
            
      if (status < 0) {
         if (errno == EINTR)
            continue;
         if (errno == EWOULDBLOCK || errno == EAGAIN)
            break;
         log_error("%s recvmsg overrun: %s\n", nl->name, safe_strerror(errno));
         continue;
      }

      else if (status == 0) {
       printf("%s EOF\n", nl->name);
         return -1;
      }

      if (msg.msg_namelen != sizeof(snl)) {
       memset(&nl_tmp, 0, MAX_SIZE);
        rc = snprintf(nl_tmp, sizeof(nl_tmp), "%s sender address length error: length %d\n", nl->name, msg.msg_namelen);
         return -1;
      }

      for (h = (struct nlmsghdr *)buf; NLMSG_OK(h, (unsigned int)status);
         h = NLMSG_NEXT(h, status)) {
         /* Finish of reading. */
         if (h->nlmsg_type == NLMSG_DONE)
            return ret;

         /* Error handling. */
         if (h->nlmsg_type == NLMSG_ERROR) {
            struct nlmsgerr *err = (struct nlmsgerr *)NLMSG_DATA(h);
            int errnum = err->error;
            int msg_type = err->msg.nlmsg_type;

            /* If the error field is zero, then this is an ACK */
            if (err->error == 0) {
#if 0
           memset(&nl_tmp, 0, MAX_SIZE);
           rc = snprintf(nl_tmp, sizeof(nl_tmp), "%s: %s ACK: type=%s(%u), seq=%u, pid=%u\n", __FUNCTION__, nl->name,
                  lookup (nlmsg_str, err->msg.nlmsg_type), err->msg.nlmsg_type, err->msg.nlmsg_seq, err->msg.nlmsg_pid);
           log_netlink_event(nl_tmp);
#endif
               /* return if not a multipart message, otherwise continue */
               if (!(h->nlmsg_flags & NLM_F_MULTI)) {
                  return 0;
               }
               continue;
            }

            if (h->nlmsg_len < NLMSG_LENGTH (sizeof (struct nlmsgerr))) {
             log_error("%s error: message truncated\n", nl->name);
               return -1;
            }
            
            memset(&nl_tmp, 0, MAX_SIZE);
           rc = snprintf(nl_tmp, sizeof(nl_tmp), "%s error: %s, type=%s(%u), seq=%u, pid=%u\n",
               nl->name, safe_strerror(-errnum),
               lookup (nlmsg_str, msg_type),
               msg_type, err->msg.nlmsg_seq, err->msg.nlmsg_pid);
           log_netlink_event(nl_tmp);

            return -1;
         }
#if 0
         /* OK we got netlink message. */           
         memset(&nl_tmp, 0, MAX_SIZE);
        rc = snprintf(nl_tmp, sizeof(nl_tmp), "netlink_parse_info: %s type %s(%u), seq=%u, pid=%u\n",
            nl->name,
            lookup (nlmsg_str, h->nlmsg_type), h->nlmsg_type,
            h->nlmsg_seq, h->nlmsg_pid);
        log_netlink_event(nl_tmp);
#endif

         error = (*filter)(&snl, h);
         if (error < 0) {           
            log_error("%s filter function error\n", nl->name);
            ret = error;
         }
      }
  
      /* After error care. */
      if (msg.msg_flags & MSG_TRUNC) {           
         log_error("%s error: message truncated\n", nl->name);
         continue;
      }
      if (status) {           
         log_error("%s error: data remnant size %d\n", nl->name, status);
         return -1;
      }
   }

   return ret;
}

int main (int argc, char **argv)
{  
   int rc __attribute__((unused)) = 0;             
   int ret __attribute__((unused)) = 0;
   int count __attribute__((unused)) = 0;
   char output[256] = {0};
   char *cad = NULL;
   pid_t sid = 0; 
   sbuf_t wired, wireless;

   // arg0 -> the whole path
   strncpy_t(arg0, 512, argv[0], strlen(argv[0]));
   
   // cad -> the basename of arg0
   if ((cad = strrchr(argv[0], '/')))
      cad++;
   else
      cad = argv[0];
      
   progname = strdup_or_null(cad);
         
   m_config = (config_t*)calloc(sizeof(config_t), 1);
   if(m_config == NULL) {
      return -ENOMEM;
   }
   
   rc = config_init(m_config);
   if(rc != 0) {
      log_error("%s: config_init: %s\n", progname, strerror(errno));
      exit(EXIT_FAILURE);
   }
   
   // parse config options from command-line 
   rc = config_load_from_args(m_config, argc, argv);      
   if(rc != 0) {
      log_error("%s: snetaid_config_load_from_argv: %s\n", progname, strerror(errno));
      config_usage(argv[0]);
      return rc;
   }
   
   // if we didn't get a config file, use the default one
   if(m_config->config_path == NULL) {
      
      m_config->config_path = strdup_or_null(DEFAULT_CONFIG_FILE);
      if(m_config->config_path == NULL) {
         // OOM 
         return -ENOMEM;
      }
   }
   
   set_debug_level(m_config->debug_level);
   set_error_level(m_config->error_level);
   
   // load from file
   rc = config_load(m_config->config_path, m_config);
   if(rc != 0) {
      log_error("%s: config_load('%s'): %s\n", progname, m_config->config_path, strerror(errno));
      exit(EXIT_FAILURE);
   }

   if (!m_config->foreground) {
     /* It is also possible to use glibc function deamon()
      * at this point, but it is useful to customize your daemon. */
      daemonize();
   }
   
   // We need an async process to monitor netlink events
   pid = fork();
   
   if( pid == 0 ) {

      pid_t sid;
      
      char *temp = rindex(m_config->nl_monitor_path, '/') + 1;
      if(strcmp(str_slice(m_config->nl_monitor_path, 0, 5), "/tmp/") == 0)
         mkdir(str_slice(m_config->nl_monitor_path, 0, strlen(m_config->nl_monitor_path) - strlen(temp) - 1), 0755);

      // detach from parent
      sid = setsid();
      if( sid < 0 ) {
         log_error("%s: setsid failed\n", progname, strerror(errno)); 
         exit(EXIT_FAILURE);
      }
  
      // ... and fork it again: the netlink monitor gets reparented to init
      pid = fork();
      
      if( pid == 0 ) {	
		  
         kernel_init();
         netlink_parse_info(netlink_information_fetch, &netlink);
         exit(0);
      }
      else if( pid > 0 ) {
         
         exit(0);
      }
      else {
       
         log_error("%s: fork(): %s\n", progname, strerror(errno));
         exit(EXIT_FAILURE);
      }
      
   } else if( pid > 0 ) {
      
      /* Open system log and write message to it */
      openlog(progname, LOG_PID|LOG_CONS, LOG_DAEMON);
      log_info("Started %s", progname);
      
      sigaction_init();
      
      alarm(1);
      
      // HACK ALERT: whilst being an exotic case, it may happen that ubus and snetaid are 
      // being supervised by different init systems -say runit and sysvinit-, in which case 
      // the latter may start before the bus, contrary to expectations.
      while(count<10) {
         FILE *pf = popen("ps -A | grep ubusd", "r");
         if(!pf) {
            printf("Sorry, there was an error opening the pipe\n");
            exit(EXIT_FAILURE);
         }
   
         if(fgets(output, sizeof(output), pf) != NULL)
            break;
         else {
            sleep(1);
            count++;
         }
         pclose(pf);
      }   
            
      uloop_init();
     
      ctx=ubus_connect(ubus_socket);
      if(ctx==NULL) exit(EXIT_FAILURE);
 
      ubus_add_uloop(ctx);
        
      ret=ubus_add_object(ctx, &netaid_obj);
      if(ret!=0) {
         log_error("%s: Ubus failure.\n", progname, strerror(errno)); 
         goto Ubus_fail;
      }
      
      /* uloop routine: events monitoring and callback provoking */
      uloop_run();
      uloop_done();
   
Ubus_fail:
      ubus_free(ctx);
      kill(pid, SIGTERM);
      exit(EXIT_FAILURE);
   
   } else {
       
      log_error("%s: fork(): %s\n", progname, strerror(errno));
      exit(EXIT_FAILURE);
   }
      
   return rc;   
}

void log_netlink_event(char *buffer)
{
   int rc __attribute__((unused)) = 0;
   int fd = -1;
   ssize_t sz = 0;
   FILE *fp = NULL;
   
   fp = fopen(m_config->nl_monitor_path, "a+");
   if (!fp) {
      log_error("%s: fopen(): %s\n", progname, strerror(errno));
      exit(EXIT_FAILURE);
   }
   
   fd = fileno(fp);
   if (fd == -1) {
      log_error("%s: fileno(): %s\n", progname, strerror(errno));
      exit(EXIT_FAILURE);
   }

   sz = write(fd, buffer, strlen(buffer));
   if(sz == -1) { 
    log_error("%s: Unable to write buffer: %s\n", progname, strerror(errno));
    exit(EXIT_FAILURE);
   }
   
   fclose(fp);
   close(fd);
}



