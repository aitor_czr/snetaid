 /*
  * util.c
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */
  
   
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>      
#include <errno.h>
#include <sys/wait.h>  // waitpid

#include <simple-netaid/sbuf.h>
#include <simple-netaid/interfaces.h>
#include <simple-netaid/helpers.h>
#include <simple-netaid/wireless.h>
#include <simple-netaid/ipaddr.h>
#include <simple-netaid/ethlink.h>
#include <simple-netaid/iproute.h>

#include "util.h"
#include "config.h"
#include "def.h"
   
#define MAX_SIZE 256
   
#define log_error_async_safe( msg ) \
   do { \
        write( STDERR_FILENO, "[snetaid error async] " msg, strlen( "[snetaid error async] " msg ) ); \
   } while(0)

// Usage: strncpy_t(str1, sizeof(str1), str2, strlen(str2));
// Copy string "in" with at most "insz" chars to buffer "out", which
// is "outsz" bytes long. The output is always 0-terminated. Unlike
// strncpy(), strncpy_t() does not zero fill remaining space in the
// output buffer:
char* strncpy_t(char* out, size_t outsz, const char* in, size_t insz)
{
   assert(outsz > 0);
   while(--outsz > 0 && insz > 0 && *in) { *out++ = *in++; insz--; }
   *out = 0;
   return out;
}

/**
 * \brief Extracts a selection of string and return a new string or NULL.
 *   It supports both negative and positive indexes.
 */
char* str_slice(char str[], int slice_from, int slice_to)
{
    // if a string is empty, returns nothing
    if (str[0] == '\0')
        return NULL;

    char *buffer;
    size_t str_len, buffer_len;

    // for negative indexes "slice_from" must be less "slice_to"
    if (slice_to < 0 && slice_from < slice_to) {
        str_len = strlen(str);

        // if "slice_to" goes beyond permissible limits
        if (abs(slice_to) > str_len - 1)
            return NULL;

        // if "slice_from" goes beyond permissible limits
        if (abs(slice_from) > str_len)
            slice_from = (-1) * str_len;

        buffer_len = slice_to - slice_from;
        str += (str_len + slice_from);

    // for positive indexes "slice_from" must be more "slice_to"
    } else if (slice_from >= 0 && slice_to > slice_from) {
        str_len = strlen(str);

        // if "slice_from" goes beyond permissible limits
        if (slice_from > str_len - 1)
            return NULL;

        buffer_len = slice_to - slice_from;
        str += slice_from;

    // otherwise, returns NULL
    } else
        return NULL;

    buffer = calloc(buffer_len, sizeof(char));
    strncpy(buffer, str, buffer_len);
    return buffer;
}
   
/**
 * \brief Create the directory to store the pidfile and the logfile.
 */
static
void mkdir_piddir(const char *s)
{
    char *piddir, *tmp;

    piddir = strdup(s);
    tmp = strrchr(piddir, '/');
    if (tmp) {
        *tmp = '\0';
        mkdir(piddir, 0755);
    }
    free(piddir);
}
   
/**
 * \brief Get the PID of the process.
 */
pid_t get_pid_t(void)
{
   static char txt[MAX_SIZE];
   int fd = -1;
   pid_t ret = (pid_t) -1, pid;
   ssize_t l;
   long lpid;
   char *e = NULL;
   
   if ((fd = open(m_config->pidfile_path, O_RDWR, 0644)) < 0) {
      if ((fd = open(m_config->pidfile_path, O_RDONLY, 0644)) < 0) {
         if (errno != ENOENT) {
         log_error("Failed to open pidfile '%s': %s , errno=%d\n", m_config->pidfile_path, strerror(errno), errno);
         }
         goto finish;
      }
   }
   
   if ((l = read(fd, txt, MAX_SIZE-1)) < 0) {
      int saved_errno = errno;
      log_error("read(): %s , errno=%d", strerror(errno), errno);
      unlink(m_config->pidfile_path);
      errno = saved_errno;
      goto finish;
   }
   
   txt[l] = 0;
   txt[strcspn(txt, "\r\n")] = 0;
   
   errno = 0;
   lpid = strtol(txt, &e, 10);
   pid = (pid_t) lpid;
   
   if (errno != 0 || !e || *e || (long) pid != lpid) {
      unlink(m_config->pidfile_path);
      errno = EINVAL;
      log_warn("PID file corrupt, removing. (%s) , errno=%d", m_config->pidfile_path, errno);
      goto finish;
   }
   
   if (kill(pid, 0) != 0 && errno != EPERM) {
      int saved_errno = errno;
      log_warn("Process %lu died: %s; trying to remove PID file. (%s)", (unsigned long) pid, strerror(errno), m_config->pidfile_path);
      unlink(m_config->pidfile_path);
      errno = saved_errno;
      goto finish;
   }
   
   ret = pid;

finish:
   if (fd != -1) {
      int rc = 0;
      if((rc=lockf(fd, F_ULOCK, 0)) < 0) {
          log_error("%s: Cannot unlock file\n", progname);
          exit(EXIT_FAILURE);
      }
      close(fd);
   }
   
   return ret;
}

/**
 * \brief Kill the daemon process.
 */
int wait_on_kill(int s, int m)
{
   pid_t pid;
   time_t t;

   if ((pid = get_pid_t()) < 0) 
      return -1;

   if (kill(pid, s) < 0)
      return -1;

   t = time(NULL) + m;

   for (;;) {
      int r;
      struct timeval tv = { 0, 100000 };

      if (time(NULL) > t) {
         errno = ETIME;
         return -1;
      }

      if ((r = kill(pid, 0)) < 0 && errno != ESRCH)
         return -1;

      if (r)
         return 0;

      if (select(0, NULL, NULL, NULL, &tv) < 0)
         return -1;
    }
}

/**
 * \brief This function will daemonize this app
 */
void daemonize()
{
   int ret;
   pid_t pid = 0;
   int fd;

   /* Fork off the parent process.
    * The first fork will change our pid
    * but the sid and pgid will be the
    * calling process
    */
   pid = fork();

   /* An error occurred */
   if (pid < 0) {
      exit(EXIT_FAILURE);
   }

   /* Fork off for the second time.
    * The magical double fork. We're the session
    * leader from the code above. Since only the
    * session leader can take control of a tty
    * we will fork and exit the session leader.
    * Once the fork is done below and we use
    * the child process we will ensure we're
    * not the session leader, thus, we cannot
    * take control of a tty.
    */
   if (pid > 0) {
      exit(EXIT_SUCCESS);
   }

   /* On success: The child process becomes session leader */
   if (setsid() < 0) {
      exit(EXIT_FAILURE);
   }

   /* Ignore signal sent from child to parent process */
   signal(SIGCHLD, SIG_IGN);

   /* Fork off for the second time*/
   pid = fork();

   /* An error occurred */
   if (pid < 0) {
      exit(EXIT_FAILURE);
   }

   /* Success: Let the parent terminate */
   if (pid > 0) {
      exit(EXIT_SUCCESS);
   }
   
   /* Set new file permissions */
   umask(0);

   /* Change the working directory to /tmp */
   /* or another appropriated directory */
   ret = chdir("/tmp");
   if(ret != 0) exit(EXIT_FAILURE);

   /* Close all open file descriptors */
   for (fd = sysconf(_SC_OPEN_MAX); fd > 0; fd--) {
      close(fd);
   }

   /* Reopen stdin (fd = 0), stdout (fd = 1), stderr (fd = 2) */
   stdin  = fopen("/dev/null", "r");
   stdout = fopen("/dev/null", "w+");
   stderr = fopen("/dev/null", "w+");
   
   /* Try to write PID of daemon to lockfile */
   if (m_config->pidfile_path)
   {
      mkdir_piddir(m_config->pidfile_path);
      
      pid_fd = open(m_config->pidfile_path, O_RDWR|O_CREAT, 0640);
      if (pid_fd < 0) { 
         log_error("%s: Cannot open pidfile: %s\n", progname, strerror(errno));
         exit(EXIT_FAILURE);
      }
      
      if (lockf(pid_fd, F_TLOCK, 0) < 0) {
         /* Can't lock file */
         exit(EXIT_FAILURE);
      }

      // Get current PID
      char *mypid = NULL;
      if(asprintf(&mypid, "%jd", (intmax_t) getpid()) != -1) {
         ssize_t sz = 0;
         // Write PID to lockfile
         sz = write(pid_fd, mypid, strlen(mypid));
         if(sz == -1) { 
            log_error("%s: Unable to write pid: %s\n", progname, strerror(errno));
            exit(EXIT_FAILURE);
         }
      } else {  
         log_error("%s: Unable to write pid: %s\n", progname, strerror(errno));
         exit(EXIT_FAILURE);
      }
   }
}

/**
 * \brief Wireless connection attempt
 */
void automatically_connect(const char *ifname)
{
   int n = 0;
   sbuf_t buffer;
   
   sbuf_init(&buffer);
    
   n=scan_active_wifis(ifname, &buffer);

   if(n) {
      const char delimiters[] = "|";
      char *token=NULL; 
      int i=0;
      while ((token = strsep(&buffer.buf, delimiters))) {
         if(i==1) {              
            struct dirent *ent;
            DIR *dir;
            
            dir = opendir(WIFI_DIR);
            if(!dir) {
               log_warn("Enable to open %s\n", WIFI_DIR);
               sbuf_free(&buffer);
               return;
               exit(EXIT_FAILURE);
            }
 
            while ((ent=readdir(dir))) {
               if (strcmp(ent->d_name,".") && strcmp(ent->d_name,"..")) {
                  if (ent->d_type != DT_DIR) {
                     FILE *fp;
                     sbuf_t path;
                     sbuf_init(&path);
                     sbuf_concat(&path, 3, WIFI_DIR, "/", ent->d_name);
                     fp = fopen(path.buf, "r");
                     if(!fp) {
                       log_error("fopen(%s): %s\n", ent->d_name, strerror(errno));
                     }
                     char line[MAX_SIZE];
                     while(fgets(line, MAX_SIZE, fp)) {
                        char *node = NULL; 
                        line[strcspn(line, "\n")] = '\0';
                        int len = strlen(line);
                        char *tmp = malloc(len + 1);
                        if(!tmp) {
                           log_error("Memory allocation failure , errno=%d\n", -ENOMEM);
                           exit(-1);
                        }
                        strncpy_t(tmp, len + 1, line, len + 1);
                        node = strtok(line, "\"");
                        if(strcmp(node, tmp)) {
                           node = strtok(NULL, "\"");
                           if(!strcmp(node, token)) {
                              int rc __attribute__((unused)) = 0;
                              sbuf_t cmd;
                              sbuf_init(&cmd);
                              kill_all_processes();
                              ipaddr_flush(ifname);
                              interface_down(ifname);
                              interface_up(ifname);
                              sbuf_concat(&cmd, 2, "/sbin/ifdown ", ifname);
                              rc = system(cmd.buf);
                              sbuf_init(&cmd);
                              sbuf_concat(&cmd, 5, "/sbin/wpa_supplicant -B -i", ifname, 
                                                   " -c\"", path.buf, "\"");
                              rc = system(cmd.buf);
                              sbuf_init(&cmd);
                              sbuf_concat(&cmd, 2, "/sbin/ifup ", ifname);
                              sleep(1);
                              rc = system(cmd.buf);                              
                              sbuf_free(&cmd);
                           }
                        } 
                        free(tmp);
                     }  
                     sbuf_free(&path);
                     fclose(fp);
                  }
               }
            } 
            closedir(dir);
         } 
         i++;
         if(i>3) i=1;
      }
   } 
   sbuf_free(&buffer);
}
