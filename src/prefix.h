#ifndef _PREFIX_H_
#define _PREFIX_H_

#ifndef HAVE_IPV6
#define HAVE_IPV6
#endif

#ifndef INET_ADDRSTRLEN
#define INET_ADDRSTRLEN 16
#endif /* INET_ADDRSTRLEN */

#ifndef INET6_ADDRSTRLEN
#define INET6_ADDRSTRLEN 46
#endif /* INET6_ADDRSTRLEN */

#ifndef INET6_BUFSIZ
#define INET6_BUFSIZ 51
#endif /* INET6_BUFSIZ */

#include <stdint.h>

/*
 *  A struct prefix contains an address family, a prefix length, and an
 *  address.  This can represent either a 'network prefix' as defined
 *  by CIDR, where the 'host bits' of the prefix are 0
 *  (e.g. AF_INET:10.0.0.0/8), or an address and netmask
 *  (e.g. AF_INET:10.0.0.9/8), such as might be configured on an
 *  interface.
 */

/* IPv4 and IPv6 unified prefix structure. */
struct prefix
{
	u_char family;
	u_char prefixlen;
	union
	{
		u_char prefix;
		struct in_addr prefix4;
#ifdef HAVE_IPV6
		struct in6_addr prefix6;
#endif /* HAVE_IPV6 */
		struct
		{
			struct in_addr id;
			struct in_addr adv_router;
		} lp;
		u_char val[8];
		uintptr_t ptr;
	} u __attribute__ ((aligned (8)));
};

/* IPv4 prefix structure. */
struct prefix_ipv4
{
	u_char family;
	u_char prefixlen;
	struct in_addr prefix __attribute__ ((aligned (8)));
};

/* IPv6 prefix structure. */
#ifdef HAVE_IPV6
struct prefix_ipv6
{
	u_char family;
	u_char prefixlen;
	struct in6_addr prefix __attribute__ ((aligned (8)));
};
#endif /* HAVE_IPV6 */


#ifndef HAVE_U_CHAR
typedef unsigned char u_char;
#define HAVE_U_CHAR
#endif /* HAVE_U_CHAR */

#ifndef DOCKER_LINUX
#define DOCKER_LINUX
#endif

#ifdef FRR
#define RTPROT_BGP        186
#define RTPROT_ISIS       187
#define RTPROT_OSPF       188
#define RTPROT_RIP        189
#define RTPROT_RIPNG      190
#if !defined(RTPROT_BABEL)
#define RTPROT_BABEL      42
#endif
#define RTPROT_NHRP       191
#define RTPROT_EIGRP      192
#define RTPROT_LDP        193
#elif defined(DOCKER_LINUX)
#define RTPROT_RIPNG	  251
#define RTPROT_OSPF6      253
#endif

#define IF_NAMSIZ         20
#define IF_HWADDR_MAX     20

static const struct message nlmsg_str[] = {
   {RTM_NEWROUTE, "RTM_NEWROUTE"},
   {RTM_DELROUTE, "RTM_DELROUTE"},
   {RTM_GETROUTE, "RTM_GETROUTE"},
   {RTM_NEWLINK,  "RTM_NEWLINK"},
   {RTM_DELLINK,  "RTM_DELLINK"},
   {RTM_GETLINK,  "RTM_GETLINK"},
   {RTM_NEWADDR,  "RTM_NEWADDR"},
   {RTM_DELADDR,  "RTM_DELADDR"},
   {RTM_GETADDR,  "RTM_GETADDR"},
   {0,            NULL}
};

static const struct message rtable_str[] = {
   {RT_TABLE_UNSPEC,   "unspecified"},
   {RT_TABLE_COMPAT,   "compat"},
   {RT_TABLE_DEFAULT,  "default"},
   {RT_TABLE_MAIN,     "main"},
   {RT_TABLE_LOCAL,    "local"},
   {RT_TABLE_MAX,      "all"},
   {0,                 NULL}
};

static const struct message rtproto_str[] = {
   {RTPROT_REDIRECT, "redirect"},
   {RTPROT_KERNEL,   "kernel"},
   {RTPROT_BOOT,     "boot"},
   {RTPROT_STATIC,   "static"},
   {RTPROT_GATED,    "GateD"},
   {RTPROT_RA,       "router advertisement"},
   {RTPROT_MRT,      "MRT"},
   {RTPROT_ZEBRA,    "Zebra"},
#ifdef RTPROT_BIRD
   {RTPROT_BIRD,     "BIRD"},
#endif /* RTPROT_BIRD */
   {RTPROT_RIP,      "rip"},
   {RTPROT_RIPNG,    "ripng"},
   {RTPROT_OSPF,     "ospf"},
   {RTPROT_OSPF6,    "ospfv3"},
   {RTPROT_BGP,      "bgp4/4+"},
   {0,               NULL}
};

static const struct message rtype_str[] = {
   {RTN_UNSPEC,       "unspecified"},
   {RTN_UNICAST,      "unicast"},
   {RTN_LOCAL,        "host"},
   {RTN_BROADCAST,    "broadcast"},
   {RTN_ANYCAST,      "anycast"},
   {RTN_MULTICAST,    "multicast"},
   {RTN_BLACKHOLE,    "blackhole"},
   {RTN_UNREACHABLE,  "unreachable"},
   {RTN_PROHIBIT,     "prohibit"},
   {RTN_THROW,        "throw"},
   {RTN_NAT,          "nat"},
   {RTN_XRESOLVE,     "xresolve"},
   {0,                NULL}
};

static const struct message rscope_str[] = {
   {RT_SCOPE_UNIVERSE,    "global"},
   {RT_SCOPE_SITE,        "AS local"},
   {RT_SCOPE_LINK,        "link local"},
   {RT_SCOPE_HOST,        "host local"},
   {RT_SCOPE_NOWHERE,     "nowhere"},
   {0,                    NULL}
};

#endif
