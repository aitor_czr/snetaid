 /*
  * util.h
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */
  
  
#ifndef __UTIL_H__
#define __UTIL_H__

#include <stdlib.h>
#include <stdbool.h>

char* str_slice(char*, int, int);
char* strncpy_t(char*, size_t, const char*, size_t);
int lock_file(int, int);
pid_t get_pid_t(void);
int wait_on_kill(int, int);
void daemonize();
void automatically_connect(const char*);

#endif  //  __UTIL_H__
