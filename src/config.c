 /*
  * config.c
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */

#include "config.h"
#include "ini.h"
#include "def.h"
#include "util.h"

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <linux/limits.h>
#include <signal.h>
   
static bool foreground_initialized = false;

// join two paths, writing the result to dest if dest is not NULL.
// otherwise, allocate and return a buffer containing the joined paths.
// return NULL on OOM 
static char* get_fullpath(char const* root, char const* path, char* dest)
{
   char delim = 0;
   int path_off = 0;
   int len = 0;
   
   if(root == NULL || path == NULL) {
      return NULL;
   }
   
   len = strlen(path) + strlen(root) + 2;

   if(strlen(root) > 0) {
      size_t root_delim_off = strlen(root) - 1;
      if(root[root_delim_off] != '/' && path[0] != '/') {
         len++;
         delim = '/';
      }
      else if(root[root_delim_off] == '/' && path[0] == '/') {
         path_off = 1;
      }
   }

   if(dest == NULL) {
      dest = (char*)calloc(sizeof(char) * len, 1);
      if(dest == NULL) {
         return NULL;
      }
   }

   memset(dest, 0, len);

   strcpy(dest, root);
   if(delim != 0) {
      dest[strlen(dest)] = '/';
   }
   strcat(dest, path + path_off);

   return dest;
}

// ini parser callback 
// return 1 on parsed (WARNING: masks OOM)
// return 0 on not parsed
static int config_ini_parser(void* userdata, char const* section, char const* name, char const* value)
{   
   config_t* conf = (config_t*)userdata;
   
   if(strcmp(section, CONFIG_NAME) == 0) {
      
      if(strcmp(name, "foreground") == 0) {
       
       // The flag --foreground does exist
         if(!foreground_initialized) {
          conf->foreground = atoi(value);
       }
         
         return 1;   
      }
      
      if(strcmp(name, CONFIG_PIDFILE_PATH) == 0) {
        
         if(conf->pidfile_path == NULL) {
            conf->pidfile_path = strdup_or_null(value);
         }
         
         return 1;
      }
      
      if(strcmp(name, CONFIG_LOGFILE_PATH) == 0) {
         
         if(conf->logfile_path == NULL) {
            conf->logfile_path = strdup_or_null(value);
         }
         
         return 1;
      }
      
      if(strcmp(name, CONFIG_NL_MONITOR_PATH) == 0) {
        
         if(conf->nl_monitor_path == NULL) {
            conf->nl_monitor_path = strdup_or_null(value);
         }
         
         return 1;
      }
      
      if(strcmp(name, CONFIG_LOG_LEVEL) == 0) {
         
         if(strcasecmp(value, "debug") == 0) {
            
            conf->debug_level = LOGLEVEL_DEBUG;
            conf->error_level = LOGLEVEL_WARN;
         }
         else if(strcasecmp(value, "info") == 0) {
            
            conf->debug_level = LOGLEVEL_INFO;
            conf->error_level = LOGLEVEL_WARN;
         }
         else if(strcasecmp(value, "warn") == 0 || strcasecmp(value, "warning") == 0) {
            
            conf->debug_level = LOGLEVEL_NONE;
            conf->error_level = LOGLEVEL_WARN;
         }
         else if(strcasecmp(value, "error") == 0 || strcasecmp(value, "critical") == 0) {
            
            conf->debug_level = LOGLEVEL_NONE;
            conf->error_level = LOGLEVEL_ERROR;
         }
         else {
            
            fprintf(stderr, "Unrecognized value '%s' for '%s'\n", value, name);
            return 0;
         }
         
         return 1;
      }
      
      return 1;
   }
   
   fprintf(stderr, "Unrecognized field '%s'\n", name);
   return 1;
}   

// initialize a config 
// always succeeds
int config_init(config_t* conf)
{   
   memset(conf, 0, sizeof(config_t));
   return 0;
}

// load from a file, by path
// return on on success
// return -errno on failure to open 
int config_load(char const* path, config_t* conf)
{   
   FILE* f = NULL;
   int rc = 0;
   
   f = fopen(path, "r");
   if(f == NULL) {
      rc = -errno;
      return rc;
   }
   
   rc = config_load_file(f, conf);
   
   fclose(f);
   
   if(rc == 0) {
      
      //config_make_instance_nonce(conf);
   }
   return rc;
}

// load from a file
// return 0 on success
// return -errno on failure to load
int config_load_file(FILE* file, config_t* conf)
{
   int rc = 0;
   
   rc = ini_parse_file(file, config_ini_parser, conf);
   if(rc != 0) {
      log_error("ini_parse_file(config) rc = %d\n", rc);
      config_free(conf);

      return rc;
   }

   // convert paths 
   rc = config_fullpaths(conf);
   if(rc != 0) {

      log_error("config_fullpaths: %s\n", strerror(-rc));
      config_free(conf);
      return rc;
   }
   
   return rc;
}

// free a config
// always succeeds
int config_free(config_t* conf)
{   
   if(conf->config_path != NULL) {
      
      free(conf->config_path);
      conf->config_path = NULL;
   }
   
   
   if(conf->logfile_path != NULL) {
      
      free(conf->logfile_path);
      conf->logfile_path = NULL;
   }
   
   if(conf->pidfile_path != NULL) {
      
      free(conf->pidfile_path);
      conf->pidfile_path = NULL;
   }
   
   if(conf->nl_monitor_path != NULL) {
      
      free(conf->nl_monitor_path);
      conf->nl_monitor_path = NULL;
   }
   
   return 0;
}

// convert all paths in the config to absolute paths 
// return 0 on success 
// return -ENOMEM on OOM 
// return -ERANGE if cwd is too long
int config_fullpaths(config_t* conf)
{   
   char** need_fullpath[] = {
      &conf->config_path,
      &conf->pidfile_path,
      &conf->logfile_path,
      &conf->nl_monitor_path,
      NULL
   };
   
   char cwd_buf[ PATH_MAX + 1 ];
   memset(cwd_buf, 0, PATH_MAX + 1);
   
   char* tmp = getcwd(cwd_buf, PATH_MAX);
   if(tmp == NULL) {
      
      log_error("Current working directory exceeds %u bytes\n", PATH_MAX);
      return -ERANGE;
   }
   
   for(int i = 0; need_fullpath[i] != NULL; i++) {
      
      if(need_fullpath[i] != NULL && (*need_fullpath[i]) != NULL) {
         
         if(*(need_fullpath[i])[0] != '/') {
               
            // relative path 
            char* new_path = get_fullpath(cwd_buf, *(need_fullpath)[i], NULL);
            if(new_path == NULL) {
               
               return -ENOMEM;
            }
            
            free(*(need_fullpath[i]));
            *(need_fullpath[i]) = new_path;
         }
      }
   }
   
   return 0;
}


// print usage statement 
int config_usage(char const* progname)
{
   fprintf(stderr, "%s -- Simple Netaid Daemon\n\n"
   "Usage: %s [options]\n\n"
      "Options:\n"
      "   -a --conf_file filename   Read configuration from the file\n\n"
      "   -l --log_file  filename   Write logs to the file\n\n"
      "   -v --verbose-level        VERBOSITY\n\
                                    Set the level of verbose output.  Valid values are\n\
                                    positive integers.  Larger integers lead to more\n\
                                    debugging information.\n\n"
      "   -p --pid_file  filename   PID file used by daemonized app\n\n"
      "   -b --background           Run in the background\n\n"
      "   -f --foreground           Run in the foreground (Default)\n\n"
      "   -s --stop                 Stop the service once the connection has been stablished\n\n"
      "   -h --help                 Show this help\n\n"
      "   -k --kill                 Kill a running daemon\n\n"
      "   -c --check-running        Check if a daemon is currently running\n\n",
      progname, progname);
  
  return 0;
}


// parse command-line options from argv.
// config must be initialized; this method simply augments it 
// return 0 on success 
// return -1 on unrecognized option 
int config_load_from_args(config_t* config, int argc, char** argv)
{      
   static struct option long_options[] = {
      // This option sets a flag.
      //{"daemon",               no_argument,  &daemonized, 1},
      // These other options don’t set a flag. We distinguish them by their indices.
      {"background",            no_argument, 0, 'b'},
      {"foreground",            no_argument, 0, 'f'},
      {"kill",                  no_argument, 0, 'k'},
      {"stop",                  no_argument, 0, 's'},
      {"verbose-level",         required_argument, 0, 'v'},
      {"config_path",           required_argument, 0, 'a'},
      {"logfile_path",          required_argument, 0, 'l'},
      {"pidfile_path",          required_argument, 0, 'p'},
      {"check-running",         no_argument, 0, 'c'},
      {0, 0, 0, 0}
   };
   
   /* Try to process all command line arguments */
   // Si la letra del argumento va seguida de : entonces es que requiere argumento,
   // si va seguida de :: entonces el argumento es opcional. El orden es irrelevante.
   // Podría ser, por ejemplo:  "a:bfkl:v:cp:"
    int rc = 0;
   int value, option_index = 0;
   int _kill = 0, _check = 0;
   while ((value = getopt_long(argc, argv, "fbka:l:v:p:c", long_options, &option_index)) != -1) {
      switch (value) {
         case 0:
            // If this option sets a flag, do nothing else now.
            if (long_options[option_index].flag)
            break;
            
         case 'b':
            m_config->foreground = false;
            foreground_initialized = true;
            break;
            
         case 'f':
            m_config->foreground = true;
            foreground_initialized = true;
            break;
            
         case 'a':
            if(m_config->config_path != NULL) {
               free(m_config->config_path);
            }
            m_config->config_path = strdup_or_null(optarg);
            break;
                
         case 'l':
            if(m_config->logfile_path != NULL) {
               free(m_config->logfile_path);
            }
            m_config->logfile_path = strdup_or_null(optarg);
            break;
         
         case 'v':
            {
               long debug_level = 0;
               char* tmp = NULL;
            
               debug_level = strtol(optarg, &tmp, 10);
            
               if(*tmp != '\0') {
                  fprintf(stderr, "Invalid argument for -d\n");
                  rc = -1;
               }
               else {
                  m_config->debug_level = debug_level;  
               }
            }
            break;
                
         case 'p':
            if(m_config->pidfile_path != NULL) {
               free(m_config->pidfile_path);
            }
            m_config->pidfile_path = strdup_or_null(optarg);
            break;
                
         case 'k':
            _kill = 1;
            break;
            
         case 'c':
            _check = 1;
            break;
                  
         default:
            syslog(LOG_ERR, "Unknown parameter.");
            rc = -1;
            break;
      }
      
   }  // while
   
   if (_kill) {
		
      int rc = 0;
      
      if ((rc = wait_on_kill(SIGTERM, 60)) < 0) {
         log_error("Failed to kill daemon with SIGTERM: %s\n", strerror(errno));
         exit(1);
      }
	        
      exit(EXIT_SUCCESS);
   }
   
   if (_check) {
      
      pid_t pid = get_pid_t();
      
       if (pid == (pid_t) -1 || pid == 0) {
          // Do not print anything here. Empty output is used in snetaid.postinst script 
          // to check whether or not sysvinit is supervising snetaid
          // printf("%s not running.\n", progname);
          exit(EXIT_FAILURE);
       } else {
          printf("%s process running as pid %u.\n", progname, pid);
          exit(EXIT_SUCCESS);
       }
   }
   
   return rc;
}

