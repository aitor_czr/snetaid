 /*
  * config.h
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */
   
#ifndef __CONFIG_H__
#define __CONFIG_H__

#include <stdio.h>
#include <getopt.h>
#include <stdbool.h>

#define CONFIG_NAME             "snetaid-config"
#define CONFIG_PIDFILE_PATH     "pidfile"
#define CONFIG_LOGFILE_PATH     "logfile"
#define CONFIG_LOG_LEVEL        "loglevel"
#define CONFIG_NL_MONITOR_PATH  "nl_monitor"

#define INI_MAX_LINE 4096
#define INI_STOP_ON_FIRST_ERROR 1

#define LOGLEVEL_DEBUG     2
#define LOGLEVEL_INFO      1
#define LOGLEVEL_NONE      0
#define LOGLEVEL_WARN      2
#define LOGLEVEL_ERROR     1

#define strdup_or_null(str)  (str) != NULL ? strdup(str) : NULL

// structure for both file configuration and command-line options
typedef struct {
   
   char *config_path;
   char *pidfile_path;
   char *logfile_path;
   char *nl_monitor_path;
   int debug_level;
   int error_level;
   bool foreground;
} config_t;

extern config_t *m_config;

int config_init(config_t*);
int config_load(char const* path, config_t*);
int config_load_file(FILE*, config_t*);
int config_free(config_t*);

int config_usage(char const*);
int config_load_from_args(config_t*, int, char**);
int config_fullpaths(config_t*);

#endif  // __CONFIG_H__
