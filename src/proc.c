 /*
  * proc.c
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>
#include <proc/readproc.h>

#include <simple-netaid/sbuf.h>
#include <pstat/libpstat.h>

#include "def.h"
#include "config.h"
#include "proc.h"
#include "util.h"

#define MAX_SIZE 1024

int kill_child(int signal)
{   
   int count = 0;   
   PROCTAB *proc = openproc(PROC_FILLSTAT);

   while (1) {
	  proc_t *proc_info = NULL;
	  proc_info = readproc(proc, NULL);	
      if(proc_info == NULL) break;
      if (!strcmp(proc_info->cmd, "snetaid") && ((proc_info->ppid == 1) && (proc_info->tgid != getpid()))) {
         bool res = false;
         res = filter_proc(proc_info->tgid);
         if(res) {
            count++;
            int child_pid = proc_info->tgid;
            if(child_pid != (uint32_t)getpid())
               kill(child_pid, signal);
         }
      }

      freeproc(proc_info);
   }
   
   closeproc(proc);
       
   return count;
}

bool filter_proc(int child_pid)
{
   char haystack[MAX_SIZE] = {0};
   char cmd[64]={0};
   FILE *pf = NULL;
   bool result_ok = false;
   char *parent = NULL;
   char *child = NULL;
   int rc;
   
   // Do they match?
   // parent -> path to the binary of the running process gotten from getpid() via pstat
   // child -> path to the binary of the process to be inspected gotten from child_pid via pstat
   // NOTE: scripts like '/etc/init.d/snetaid' will return "/bin/dash", 
   //       "/bin/bash", and so on, depending on the shebang.
   rc = pstat_get_binary_path_from_integer(&parent, 128, (uint32_t)getpid());     
   if(rc!=0) {
	  free(parent);
	  exit(EXIT_FAILURE);
   }
   rc = pstat_get_binary_path_from_integer(&child, 128, child_pid);     
   if(rc!=0) {
	  free(child);
	  exit(EXIT_FAILURE);
   }
   if(strcmp(parent, child)) {
      free(parent);
      free(child);
      return false;
   }
   free(parent);
   
   // They match, however we won't blithely kill the process
   // Make sure it depends on libnetaid, before killing it.
   // This step might be redundant, as we already have compared 
   // both paths. But go ahead anyway...
   strncpy_t(cmd, sizeof(cmd), "ldd ", 4);
   strcat(cmd, child);
   strcat(cmd, " | grep libnetaid.so");
   pf = popen(cmd, "r");
   if(!pf) { 
      free(child);
      log_error("%s: popen(): %s\n", progname, strerror(errno));
      exit(EXIT_FAILURE);
   }   
   if(fgets(haystack, MAX_SIZE, pf)) {
      // The following verification is recommended because ldd might return a 
      // warning. In such case, we would get a non empty output.
      const char *needle = "libnetaid.so";
      if(strstr(haystack, needle)) result_ok = true;
   }
   
   free(child);
   pclose(pf);
      
   return result_ok;
}
